import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/shared/pick_camera_screen.dart';

class CameraPicker {
  static Future<File> pickImage(BuildContext context) async {
    var result = await _getResultFromCameraView(context);
    return result;
  }

  static _getResultFromCameraView(BuildContext context) async {
    return Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return PickCameraScreen();
      }),
    );
  }
}
