import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:real_estate/localization/flutter_localizations.dart';
import 'package:real_estate/localization/text_names.dart';
import 'package:real_estate/theme/theme.dart';

class PickCameraScreen extends StatefulWidget {
  @override
  _PickCameraScreenState createState() => _PickCameraScreenState();
}

class _PickCameraScreenState extends State<PickCameraScreen> {
  CameraController cameraController;

  void _initializeCamera() async {
    var cameras = await availableCameras();
    cameraController = CameraController(cameras.first, ResolutionPreset.max,
        enableAudio: false);
    try {
      await cameraController.initialize();
    } on CameraException catch (e) {
      print(e.toString());
    }
    if (!mounted) {
      return;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    _initializeCamera();
  }

  @override
  void dispose() {
    cameraController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: AppThemes.defaultTheme,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Theme.of(context).backgroundColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              size: 24,
              color: AppColors.black,
            ),
            onPressed: () => Navigator.maybePop(context),
          ),
        ),
        body: SafeArea(
          child: _buildContent(context),
        ),
      ),
    );
  }

  _buildContent(BuildContext context) {
    if (cameraController == null || !cameraController.value.isInitialized) {
      return _buildLoading();
    }

    if (cameraController.value.hasError) {
      return _buildErrorCameraView(context);
    }

    return _buildCameraView(context);
  }

  _buildCameraView(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            color: AppColors.white,
            child: Transform.scale(
              scale: 1 / cameraController.value.aspectRatio,
              child: Center(
                child: AspectRatio(
                    aspectRatio: cameraController.value.aspectRatio,
                    child: CameraPreview(cameraController)),
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(AppDimens.spaceMedium),
          color: Theme.of(context).backgroundColor,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                width: 64,
                height: 64,
                child: cameraController.value.isTakingPicture
                    ? _buildLoading()
                    : _buttonTakePhoto(context),
              ),
            ],
          ),
        )
      ],
    );
  }

  _buttonTakePhoto(BuildContext context) {
    return ClipOval(
      child: Material(
        color: Theme.of(context).accentColor, // button color
        child: InkWell(
          child: ClipOval(
            child: Container(
              padding: EdgeInsets.all(AppDimens.spaceMedium / 2),
              child: ClipOval(
                child: Container(
                  color: Theme.of(context).backgroundColor,
                ),
              ),
            ),
          ),
          onTap: () {
            _takePhoto(context);
          },
        ),
      ),
    );
  }

  _buildErrorCameraView(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(AppDimens.spaceLarge),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                NPLocalizations.of(context)
                    .getStringLabel(context, TextName.lbErrorCamera),
                maxLines: 5),
            FlatButton(
              child: Text(NPLocalizations.of(context)
                  .getStringLabel(context, TextName.lbRetry)),
              onPressed: onPressedRetry,
            )
          ],
        ),
      ),
    );
  }

  void onPressedRetry() async {
    await cameraController.dispose();
    setState(() {});
    _initializeCamera();
  }

  _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void _takePhoto(BuildContext context) async {
    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      var path = join((await getTemporaryDirectory()).path,
          'np_item_${DateTime.now().millisecondsSinceEpoch}.png');
      cameraController.takePicture(path).then((value) {
        Navigator.of(context).pop(File(path));
      });
      setState(() {});
    } on CameraException catch (e) {
      print(e.toString());
      setState(() {});
    }
  }
}
