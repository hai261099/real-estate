import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'app/app.dart';
import 'app/app_config.dart';

void main() {
  //debug config
  AppConfig(config: AppConfigValues.STAGING);
  WidgetsFlutterBinding.ensureInitialized();

  runApp(Application());
}
