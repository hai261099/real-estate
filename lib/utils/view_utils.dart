import 'package:flutter/material.dart';
import 'package:real_estate/core/components/np_error_dialog.dart';
import 'package:real_estate/localization/flutter_localizations.dart';
import 'package:real_estate/localization/text_names.dart';
import 'package:real_estate/theme/theme.dart';

showDialogError(BuildContext context,
    {String msg, Function tryAgainCallBack, Function closeCallBack}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) => NPErrorDialog(
      title: NPLocalizations.of(context)
          .getStringLabel(context, TextName.lbTitleError),
      message: msg != null
          ? NPLocalizations.of(context).getStringError(msg)
          : NPLocalizations.of(context)
              .getStringLabel(context, TextName.errorUnknown),
      btnTryAgainPress: () {
        Navigator.pop(context);
        if (tryAgainCallBack != null) {
          tryAgainCallBack();
        }
      },
      btnClosePress: () {
        Navigator.pop(context);
        if (closeCallBack != null) {
          tryAgainCallBack();
        }
      },
    ),
  );
}

Future<void> showProcessDialog(BuildContext context) async {
  await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (_) => WillPopScope(
      onWillPop: () async => false,
      child: new AlertDialog(
        content: Row(
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(width: AppDimens.spaceMedium),
            Text(
              NPLocalizations.of(context)
                  .getStringLabel(context, TextName.loading),
            ),
          ],
        ),
      ),
    ),
  );
}
