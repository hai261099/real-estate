
import 'package:shared_preferences/shared_preferences.dart';

import 'app_const.dart';

Future<bool> saveLanguageResourceFromRemote(
    String languageCode, String lang) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.setString(
      '${Const.PACKAGE_APP_PATH}/strings/$languageCode', lang);
}

Future<String> getLanguageResourceFromRemote(String languageCode) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.get('${Const.PACKAGE_APP_PATH}/strings/$languageCode');
}
