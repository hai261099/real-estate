import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:real_estate/theme/theme.dart';

import 'np_textfield_custom/np_textfield_custom.dart';
import 'np_textfield_custom/np_input_decorator.dart';

class NPTextField extends StatefulWidget {
  final String labelText;
  final String hintText;
  final String errorText;
  final String helperText;
  final Color borderColor;
  final Color backgroundColor;
  final double heightTextField;
  final bool obscureText;
  final bool autoFocus;
  final TextInputType keyboardType;
  final FocusNode focusNode;
  final TextEditingController controller;
  final Function(String) onChangedText;
  final Function(bool) onChangedFocus;
  final List<Widget> leftIcons;
  final int maxLines;
  final int minLines;
  final int maxLength;
  final bool enabled;
  final bool labelAlways;
  final List<TextInputFormatter> inputFormatters;
  final bool enableInteractiveSelection;

  NPTextField(
      {this.labelText = '',
      this.hintText = '',
      this.errorText,
      this.helperText,
      this.backgroundColor = AppColors.white500,
      this.borderColor,
      this.obscureText = false,
      this.onChangedText,
      this.focusNode,
      this.heightTextField = AppDimens.heightTextField,
      this.controller,
      this.maxLines = 1,
      this.minLines,
      this.maxLength,
      this.autoFocus = false,
      this.keyboardType = TextInputType.text,
      this.leftIcons,
      this.enabled = true,
      this.labelAlways = true,
      this.inputFormatters,
      this.onChangedFocus,
      this.enableInteractiveSelection = true});

  @override
  _NPTextFieldState createState() => _NPTextFieldState();
}

class _NPTextFieldState extends State<NPTextField> {
  var focusNode;

  @override
  void initState() {
    // TODO: implement initState
    if (widget.focusNode != null) {
      focusNode = widget.focusNode;
    } else {
      focusNode = FocusNode();
    }
    focusNode.addListener(() {
      if (widget.onChangedFocus != null) {
        widget.onChangedFocus(focusNode.hasFocus);
      }
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_textFieldCustom(), SizedBox(height: 4), _bottomWidget()],
      ),
    );
  }

  Widget _textFieldCustom() {
    var colorBorder = (widget.errorText != null)
        ? AppColors.red500
        : widget.borderColor ?? AppColors.ink300;

    return Container(
        decoration: BoxDecoration(
          border: Border.all(
              width: 1,
              color: focusNode.hasFocus
                  ? widget.borderColor ?? AppColors.primaryA500
                  : colorBorder),
          color: (widget.enabled) ? AppColors.white500 : AppColors.ink200,
          borderRadius:
              new BorderRadius.all(Radius.circular(AppDimens.radiusMedium)),
        ),
        padding: EdgeInsets.only(
            top: 0,
            bottom: 0,
            left: AppDimens.spaceMedium,
            right: AppDimens.spaceMedium),
        height: widget.heightTextField,
        child: Row(
          children: [
            Expanded(
              child: _buildLeftCustomTextField(),
            ),
            _buildRightCustomTextField()
          ],
        ));
  }

  Widget _buildRightCustomTextField() {
    if (widget.leftIcons == null || widget.leftIcons.length == 0) {
      return Container();
    }
    var listChildren = List<Widget>();
    widget.leftIcons.forEach((element) {
      listChildren.add(Container(
          child: element,
          width: AppDimens.heightIconSize,
          height: AppDimens.heightIconSize));
      if (listChildren.length < widget.leftIcons.length) {
        listChildren.add(Padding(padding: EdgeInsets.only(left: 2, right: 2)));
      }
    });
    return Container(
        child: Row(
      children: listChildren,
    ));
  }

  Widget _buildLeftCustomTextField() {
    List<TextInputFormatter> inputFormatter =
        widget.inputFormatters ?? new List();
    inputFormatter.add(FilteringTextInputFormatter.deny(RegExp(r'\s+\s+'),
        replacementString: ' '));
    return NPTextFieldCustom(
      style: AppTextStyles.regularMedium(context, color: AppColors.ink500),
      controller: widget.controller,
      focusNode: focusNode,
      onChanged: widget.onChangedText,
      obscureText: widget.obscureText,
      maxLines: widget.maxLines,
      minLines: widget.minLines,
      keyboardType: widget.keyboardType,
      autofocus: widget.autoFocus,
      enabled: widget.enabled,
      maxLength: widget.maxLength,
      textCapitalization: TextCapitalization.sentences,
      inputFormatters: inputFormatter,
      enableInteractiveSelection: widget.enableInteractiveSelection,
      decoration: new NPInputDecoration(
        counterText: '',
        labelText: widget.labelText,
        labelStyle:
            AppTextStyles.regularMedium(context, color: AppColors.ink400),
        hintText: widget.hintText,
        hintStyle:
            AppTextStyles.regularMedium(context, color: AppColors.ink400),
        floatingLabelBehavior: widget.labelAlways
            ? FloatingLabelBehavior.always
            : FloatingLabelBehavior.auto,
        contentPadding: EdgeInsets.all(0.0),
        isDense: true,
        border: InputBorder.none,
      ),
    );
  }

  Widget _bottomWidget() {
    if ((widget.errorText != null && widget.errorText.length > 0) ||
        (widget.helperText != null && widget.helperText.length > 0)) {
      return Padding(
        padding: const EdgeInsets.only(left: AppDimens.spaceMedium),
        child: Text(
          widget.errorText != null ? widget.errorText : widget.helperText,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: AppTextStyles.regularXSmall(context,
              color: widget.errorText != null
                  ? AppColors.red500
                  : AppColors.ink400),
        ),
      );
    }
    return Container();
  }
}
