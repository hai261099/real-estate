import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';

class NPStepper extends StatefulWidget {
  final StepperSize size;
  final Function(int) onChanged;
  final int maxValue;
  int value;

  NPStepper(
      {this.size = StepperSize.large,
      this.onChanged,
      this.value = 1,
      this.maxValue = Const.maxItem});

  @override
  _NPStepperState createState() => _NPStepperState();
}

class _NPStepperState extends State<NPStepper> {
  var space = 8.0;
  Timer timer;
  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: '${widget.value}');
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        if (!visible) {
          var text = controller.text;
          if (text.length == 0) {
            text = '${Const.numItemDefault}';
            controller.text = text;
          }
          setState(() {
            widget.value = int.parse(text);
          });

          if (widget.onChanged != null) {
            widget.onChanged(widget.value);
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var height = sizeStepperByType();
    controller.text = '${widget.value}';
    return Container(
        height: height,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
              onTapDown: (TapDownDetails details) {
                timer = Timer.periodic(Duration(milliseconds: 250), (t) {
                  if (widget.value > 1) {
                    setState(() {
                      widget.value--;
                    });
                    controller.text = '${widget.value}';
                  }
                  if (widget.onChanged != null) {
                    widget.onChanged(widget.value);
                  }
                });
              },
              onTapUp: (TapUpDetails details) {
                if (timer != null) {
                  timer.cancel();
                }
              },
              onTapCancel: () {
                if (timer != null) {
                  timer.cancel();
                }
              },
              child: SizedBox(
                width: height,
                height: height,
                child: RawMaterialButton(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(AppDimens.radiusMedium),
                      side: BorderSide(color: AppColors.ink300)),
                  child: Icon(Icons.remove,
                      size: AppDimens.heightIconSize,
                      color: widget.value == 1
                          ? AppColors.ink400
                          : AppColors.primaryA500),
                  onPressed: () {
                    if (widget.value > 1) {
                      setState(() {
                        widget.value--;
                      });
                      controller.text = '${widget.value}';
                    }
                    if (widget.onChanged != null) {
                      widget.onChanged(widget.value);
                    }
                  },
                  highlightColor: AppColors.white200,
                  splashColor: AppColors.ink100,
                  fillColor: AppColors.white500,
                ),
              ),
            ),
            // Space
            SizedBox(width: space),
            SizedBox(
              width: height,
              height: height,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: AppColors.ink300),
                  color: AppColors.white500,
                  borderRadius: new BorderRadius.all(
                      Radius.circular(AppDimens.radiusMedium)),
                ),
                child: Center(
                  child: TextField(
                      textAlign: TextAlign.center,
                      style: AppTextStyles.regularMedium(context,
                          color: AppColors.ink500),
                      keyboardType: TextInputType.phone,
                      controller: controller,
                      onChanged: (newVal) {
                        if (newVal.length == 0) {
                          return;
                        } else if (newVal.length == 1 &&
                            int.parse(newVal) == 0) {
                          newVal = '1';
                          controller.text = newVal;
                        }
                        widget.value = int.parse(newVal);

                        if (widget.onChanged != null) {
                          widget.onChanged(widget.value);
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      maxLength: 2,
                      decoration: new InputDecoration(
                        counterText: '',
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                            bottom: 0, top: 0, right: 0, left: 2),
                      )),
                ),
              ),
            ),

            // Space
            SizedBox(width: space),
            GestureDetector(
              onTapDown: (TapDownDetails details) {
                timer = Timer.periodic(Duration(milliseconds: 250), (t) {
                  if (widget.value >= widget.maxValue) {
                    return;
                  }
                  setState(() {
                    widget.value++;
                  });
                  controller.text = '${widget.value}';
                  if (widget.onChanged != null) {
                    widget.onChanged(widget.value);
                  }
                });
              },
              onTapUp: (TapUpDetails details) {
                if (timer != null) {
                  timer.cancel();
                }
              },
              onTapCancel: () {
                if (timer != null) {
                  timer.cancel();
                }
              },
              child: SizedBox(
                width: height,
                height: height,
                child: RawMaterialButton(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(AppDimens.radiusMedium),
                      side: BorderSide(color: AppColors.ink300)),
                  child: Icon(
                    Icons.add,
                    size: AppDimens.heightIconSize,
                    color: AppColors.primaryA500,
                  ),
                  onPressed: () {
                    if (widget.value >= widget.maxValue) {
                      return;
                    }
                    setState(() {
                      widget.value++;
                    });
                    controller.text = '${widget.value}';
                    if (widget.onChanged != null) {
                      widget.onChanged(widget.value);
                    }
                  },
                  highlightColor: AppColors.white200,
                  splashColor: AppColors.ink100,
                  constraints: BoxConstraints.tightFor(
                    width: height,
                    height: height,
                  ),
                  fillColor: AppColors.white500,
                ),
              ),
            ),
          ],
        ));
  }

  double sizeStepperByType() {
    // size Normal
    double result = 0;
    switch (widget.size) {
      case StepperSize.large:
        result = AppDimens.stepperSizeLarge;
        space = 8.0;
        break;
      case StepperSize.medium:
        result = AppDimens.stepperSizeMedium;
        space = 8.0;
        break;
      case StepperSize.small:
        result = AppDimens.stepperSizeSmall;
        space = 16.0;
        break;
    }
    return result;
  }
}
