import 'package:flutter/material.dart';
import 'package:real_estate/assets/images/images.dart';
import 'package:real_estate/localization/flutter_localizations.dart';
import 'package:real_estate/localization/text_names.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';


import 'np_button.dart';

class NPErrorDialog extends StatefulWidget {
  final String title;
  final String message;
  final Function btnTryAgainPress;
  final Function btnClosePress;

  const NPErrorDialog({
    Key key,
    this.title,
    this.message,
    this.btnTryAgainPress,
    this.btnClosePress,
  }) : super(key: key);
  @override
  _NPErrorDialogState createState() => _NPErrorDialogState();
}

class _NPErrorDialogState extends State<NPErrorDialog> {
  double dialogWidth = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    dialogWidth = 0.9 * width;

    return Dialog(
      insetPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          AppDimens.radiusMedium,
        ),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          AppDimens.radiusMedium,
        ),
        color: Colors.white,
      ),
      width: dialogWidth,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    AppDimens.radiusMedium,
                  ),
                  color: Colors.white,
                ),
                width: double.infinity,
                padding: EdgeInsets.all(
                  AppDimens.spaceMedium,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: AppDimens.spaceMedium,
                        bottom: AppDimens.spaceMedium,
                      ),
                      child: Center(
                        child: Image.asset(
                          AppUtils.assetImage(Images.icError),
                          width: 128,
                          height: 128,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        widget.title,
                        textAlign: TextAlign.center,
                        style: AppTextStyles.medium(
                          context,
                          color: AppColors.black500,
                          size: AppDimens.textSizeXLarge20,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: AppDimens.spaceMedium,
                        bottom: AppDimens.spaceMedium,
                      ),
                      child: Center(
                        child: Text(
                          widget.message,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: AppTextStyles.regularSmall(
                            context,
                            color: AppColors.ink400,
                          ),
                        ),
                      ),
                    ),
                    NPButton(
                      backgroundColor: AppColors.blue500,
                      title: NPLocalizations.of(context)
                          .getStringLabel(context, TextName.btnTryAgain),
                      onPressed: widget.btnTryAgainPress,
                    ),
                    NPButton(
                      backgroundColor: Colors.transparent,
                      titleColor: AppColors.blue500,
                      title: NPLocalizations.of(context)
                          .getStringLabel(context, TextName.btnClose),
                      onPressed: widget.btnClosePress,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    size: 26,
                    color: AppColors.black500,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
