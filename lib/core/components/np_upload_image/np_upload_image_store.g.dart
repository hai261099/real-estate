// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'np_upload_image_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NPUploadImageStore on _NPUploadImageStore, Store {
  final _$isLoadingAtom = Atom(name: '_NPUploadImageStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$isErrorAtom = Atom(name: '_NPUploadImageStore.isError');

  @override
  bool get isError {
    _$isErrorAtom.reportRead();
    return super.isError;
  }

  @override
  set isError(bool value) {
    _$isErrorAtom.reportWrite(value, super.isError, () {
      super.isError = value;
    });
  }

  final _$imageUploadAtom = Atom(name: '_NPUploadImageStore.imageUpload');

  @override
  File get imageUpload {
    _$imageUploadAtom.reportRead();
    return super.imageUpload;
  }

  @override
  set imageUpload(File value) {
    _$imageUploadAtom.reportWrite(value, super.imageUpload, () {
      super.imageUpload = value;
    });
  }

  final _$retryUploadAsyncAction =
      AsyncAction('_NPUploadImageStore.retryUpload');

  @override
  Future<dynamic> retryUpload(
      dynamic Function(bool, String) completionHandler) {
    return _$retryUploadAsyncAction
        .run(() => super.retryUpload(completionHandler));
  }

  final _$uploadImageAsyncAction =
      AsyncAction('_NPUploadImageStore.uploadImage');

  @override
  Future<dynamic> uploadImage(
      File file, dynamic Function(bool, String) completionHandler) {
    return _$uploadImageAsyncAction
        .run(() => super.uploadImage(file, completionHandler));
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
isError: ${isError},
imageUpload: ${imageUpload}
    ''';
  }
}
