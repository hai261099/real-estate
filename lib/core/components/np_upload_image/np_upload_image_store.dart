import 'dart:developer';
import 'dart:io';

import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:mobx/mobx.dart';
import 'package:real_estate/core/api_helper/api_helper/common_api.dart';


part 'np_upload_image_store.g.dart';

class NPUploadImageStore = _NPUploadImageStore with _$NPUploadImageStore;

abstract class _NPUploadImageStore with Store {
  _NPUploadImageStore();

  final maxFileSizeMB = 2;

  @observable
  bool isLoading = false, isError = false;

  @observable
  File imageUpload;

  @action
  Future<dynamic> retryUpload(
      dynamic Function(bool isSuccess, String imagePath)
          completionHandler) async {
    uploadImage(imageUpload, completionHandler);
  }

  @action
  Future<dynamic> uploadImage(
      File file,
      dynamic Function(bool isSuccess, String imagePath)
          completionHandler) async {
    try {
      if (isLoading) {
        return;
      }
      isLoading = true;
      isError = false;
      File compressedFile = await this.compressedFile(file);
      imageUpload = compressedFile;
      var response = await CommonApi.uploadFile([compressedFile]);
      isLoading = false;
      if (response != null && response.success) {
        var mapData = response.data as Map;
        if (mapData != null) {
          var imagePath = mapData['imageLink'];
          if (completionHandler != null) {
            completionHandler(true, imagePath);
          }
          return;
        }
      }
      log('Data failure');
      isError = true;
      completionHandler(false, null);
    } catch (e) {
      isError = true;
      isLoading = false;
      log('Exception: ${e.toString()}');
      completionHandler(false, null);
    }
  }

  Future<File> compressedFile(File file) async {
    // Get length of file in bytes
    var fileSizeInBytes = file.lengthSync();
    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
    double fileSizeInKB = fileSizeInBytes / 1024;
    log(fileSizeInKB.toString() + 'KB');
    if (fileSizeInKB > maxFileSizeMB * 1024) {
      File compressedFile = await FlutterNativeImage.compressImage(file.path,
          quality: 70, percentage: 70);
      return this.compressedFile(compressedFile);
    } else {
      return file;
    }
  }
}
