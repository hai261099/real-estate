// import 'dart:io';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_mobx/flutter_mobx.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:real_estate/assets/images/images.dart';
// import 'package:real_estate/localization/text_names.dart';
// import 'package:real_estate/shared/camera_picker.dart';
// import 'package:real_estate/utils/app_utils.dart';

// import '../../../theme/app_text_styles.dart';
// import '../../../theme/theme.dart';
// import 'np_upload_image_store.dart';

// class NPUploadImage extends StatelessWidget {
//   final String image;
//   final double iconSize;
//   final bool hasRemoveIcon;
//   final Function(String) onDidUploadFile;
//   final Function onRemoveUploadFile;
//   final NPUploadImageStore uploadImageStore;

//   NPUploadImage(
//       {@required this.image,
//       this.iconSize,
//       this.hasRemoveIcon = false,
//       this.onDidUploadFile,
//       this.onRemoveUploadFile,
//       this.uploadImageStore}) {}

//   @override
//   Widget build(BuildContext context) {
//     return ClipRRect(
//       borderRadius: BorderRadius.circular(AppDimens.radiusSmall),
//       child: Container(
//         width: iconSize,
//         height: iconSize,
//         child: Stack(
//           children: <Widget>[
//             InkWell(
//               onTap: () {
//                 onTapAddNewImage(context);
//               },
//               child: Observer(builder: (_) {
//                 if (image != null && image.length > 0) {
//                   return _imageNetwork();
//                 } else if (uploadImageStore.imageUpload != null) {
//                   if (!uploadImageStore.isError) {
//                     return _imageWithFile();
//                   }
//                   return _imageUploadFail(context);
//                 } else {
//                   return _imageNone();
//                 }
//               }),
//             ),
//             hasRemoveIcon ? _buidButtonRemove(context) : Container(),
//             Center(
//               child: Observer(builder: (_) {
//                 if (uploadImageStore.isLoading) {
//                   return CircularProgressIndicator();
//                 } else {
//                   return Container();
//                 }
//               }),
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _buidButtonRemove(BuildContext context) {
//     if (image == null) {
//       return Container();
//     }
//     return Align(
//       alignment: Alignment.topRight,
//       child: _removeButton(context),
//     );
//   }

//   Widget _imageWithFile() {
//     return Container(
//       width: double.infinity,
//       height: double.infinity,
//       child: Image.file(
//         uploadImageStore.imageUpload,
//         fit: BoxFit.cover,
//       ),
//     );
//   }

//   Widget _imageUploadFail(BuildContext context) {
//     return Container(
//       width: double.infinity,
//       height: double.infinity,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.all(Radius.circular(8)),
//       ),
//       child: Stack(
//         children: <Widget>[
//           Image.file(
//             uploadImageStore.imageUpload,
//             fit: BoxFit.cover,
//           ),
//           Container(
//             height: double.infinity,
//             width: double.infinity,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.all(Radius.circular(8)),
//                 color: Theme.of(context).errorColor.withOpacity(0.6)),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 Text(
//                   'Upload failed!',
//                   style: AppTextStyles.regularXSmall(context,
//                       color: AppColors.white500),
//                 ),
//                 FlatButton(
//                   onPressed: () {
//                     uploadImageStore
//                         .retryUpload((bool isSuccess, String imgPath) {
//                       if (isSuccess && onDidUploadFile != null) {
//                         onDidUploadFile(imgPath);
//                       }
//                     });
//                   },
//                   child: Icon(
//                     Icons.refresh,
//                     color: Theme.of(context).primaryIconTheme.color,
//                   ),
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   Widget _imageNetwork() {
//     return ClipRRect(
//       borderRadius: BorderRadius.circular(AppDimens.radiusMedium),
//       child: Container(
//           width: double.infinity,
//           height: double.infinity,
//           child: CachedNetworkImage(
//             fit: BoxFit.cover,
//             imageUrl: image ?? '',
//             placeholder: (context, url) {
//               return Container(
//                 child: Center(
//                   child: CircularProgressIndicator(),
//                 ),
//               );
//             },
//             errorWidget: (context, url, objext) {
//               return _buildDefaultImage();
//             },
//           )
//           // decoration: BoxDecoration(
//           //     borderRadius: BorderRadius.all(Radius.circular(8)),
//           //     image: DecorationImage(
//           //         fit: BoxFit.cover, image: CachedNetworkImageProvider(image))),
//           ),
//     );
//   }

//   Widget _buildDefaultImage() {
//     return Image.asset(AppUtils.assetImage(Images.itemImageDefault),
//         width: iconSize, height: iconSize);
//   }

//   Widget _imageNone() {
//     return Container(
//       decoration: BoxDecoration(
//         color: Color(0xFFF2F2F4),
//         borderRadius: BorderRadius.all(Radius.circular(8)),
//       ),
//       child: Center(
//         child: Image.asset(AppUtils.assetImage(Images.icUploadImage)),
//       ),
//     );
//   }

//   _removeButton(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         uploadImageStore.imageUpload = null;
//         if (onRemoveUploadFile != null) onRemoveUploadFile();
//       },
//       child: Padding(
//         padding: EdgeInsets.all(4),
//         child: ClipRRect(
//           borderRadius: BorderRadius.all(Radius.circular(16)),
//           child: Container(
//             height: 20,
//             width: 20,
//             color: Theme.of(context).hoverColor,
//             child: Center(
//               child: Icon(Icons.close,
//                   size: 14, color: Theme.of(context).accentColor),
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   onTapAddNewImage(BuildContext context) {
//     final act = CupertinoActionSheet(
//         title: Text(
//             AppUtils.npLocalization(context, TextName.lbDialogAddImageTitle)),
//         actions: <Widget>[
//           CupertinoActionSheetAction(
//             child:
//                 Text(AppUtils.npLocalization(context, TextName.lbActionPhotos)),
//             onPressed: () {
//               Navigator.pop(context);
//               _getImagePicker(true, context);
//             },
//           ),
//           CupertinoActionSheetAction(
//             child:
//                 Text(AppUtils.npLocalization(context, TextName.lbActionCamera)),
//             onPressed: () {
//               Navigator.pop(context);
//               _getImagePicker(false, context);
//             },
//           )
//         ],
//         cancelButton: CupertinoActionSheetAction(
//           child:
//               Text(AppUtils.npLocalization(context, TextName.lbActionCancel)),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ));
//     showCupertinoModalPopup(
//         context: context,
//         builder: (BuildContext context) =>
//             Theme(data: AppThemes.defaultTheme, child: act));
//   }

//   Future _getImagePicker(bool isPhoto, BuildContext context) async {
//     String image;
//     if (isPhoto) {
//       image = (await ImagePicker.pickImage(source: ImageSource.gallery))?.path;
//     } else {
//       image = (await CameraPicker.pickImage(context))?.path;
//     }
//     if (image == null || image.isEmpty) {
//       return;
//     }
//     if (onDidUploadFile != null)
//       uploadImageStore.uploadImage(File(image),
//           (bool isSuccess, String imgPath) {
//         if (isSuccess) {
//           onDidUploadFile(imgPath);
//         }
//       });
//   }
// }
