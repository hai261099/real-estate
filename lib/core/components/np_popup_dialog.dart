import 'package:flutter/material.dart';
import 'package:real_estate/localization/flutter_localizations.dart';
import 'package:real_estate/localization/text_names.dart';
import 'package:real_estate/theme/theme.dart';

import 'np_divider.dart';

class NPPopupDialog extends StatelessWidget {
  NPPopupDialog({
    this.title,
    this.children,
    this.onSelected,
  });

  final Widget title;
  final List<Widget> children;
  final Function(int) onSelected;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: AppThemes.defaultTheme,
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(16.0),
            topRight: const Radius.circular(16.0),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 54,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(16.0),
                  topRight: const Radius.circular(16.0),
                ),
              ),
              child: Stack(
                children: [
                  Center(
                    child: title ??
                        Text(
                          NPLocalizations.of(context)
                              .getStringLabel(context, TextName.lbAddNew),
                          style: Theme.of(context).textTheme.headline6.copyWith(
                                color: AppColors.ink500,
                                fontWeight: FontWeight.w500,
                                fontSize: AppDimens.textSizeMedium,
                              ),
                        ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      onPressed: Navigator.of(context).pop,
                      icon: Icon(
                        Icons.close,
                        color: AppColors.ink500,
                      ),
                    ),
                  )
                ],
              ),
            ),
            NPDivider(
              color: AppColors.divider,
              height: 0,
            ),
            ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => children[index],
              separatorBuilder: (context, index) => Padding(
                padding: const EdgeInsets.only(left: AppDimens.spaceMedium),
                child: NPDivider(),
              ),
              itemCount: (children != null) ? children.length : 0,
            ),
          ],
        ),
      ),
    );
  }
}
