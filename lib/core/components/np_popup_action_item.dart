import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';

class NPPopupActionItem extends StatelessWidget {
  final String iconAsset;

  final String title;

  final String subTitle;

  final VoidCallback onPress;

  const NPPopupActionItem(
      {Key key, this.iconAsset, this.title, this.subTitle, this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        padding: EdgeInsets.all(AppDimens.spaceMedium),
        child: Row(
          children: [
            (iconAsset != null)
                ? Image.asset(
                    AppUtils.assetImage(iconAsset),
                    width: 20,
                    height: 20,
                  )
                : Container(),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: AppTextStyles.regularSmall(
                      context,
                    ),
                  ),
                  (subTitle != null)
                      ? Text(
                          subTitle,
                          style: AppTextStyles.regularXSmall(
                            context,
                            color: AppColors.ink400,
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
