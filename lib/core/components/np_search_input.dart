import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/ui_utils.dart';


typedef SearchListener = void Function({String text});

class NPSearchInput extends StatefulWidget {
  final String hintText;
  final SearchListener onChanged;
  final SearchListener onDone;
  final Function onPress;
  final bool isAutoFocus;
  final bool showCloseButton;
  final Color backgroundColor;

  NPSearchInput({
    this.hintText,
    this.onChanged,
    this.onDone,
    this.onPress,
    this.isAutoFocus = true,
    this.showCloseButton = false,
    this.backgroundColor = const Color(0xFFF0F6FF),
  });

  @override
  _NPSearchInputState createState() => _NPSearchInputState();
}

class _NPSearchInputState extends State<NPSearchInput> {
  DeBouncer deBouncer;
  bool _showCloseButton = false;
  TextEditingController textController = TextEditingController();

  _changedListener(String text) {
    if (widget.onChanged != null) {
      widget.onChanged(text: text);
    }
  }

  _doneListener(String text) {
    hideKeyboard(context);
    if (widget.onDone != null) {
      widget.onDone(text: text);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    deBouncer =
        DeBouncer<String>(Duration(milliseconds: 250), _changedListener);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: AppThemes.defaultTheme,
      child: InkWell(
        onTap: widget.onPress,
        child: Container(
          decoration: BoxDecoration(
              color: widget.backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(8))),

          alignment: Alignment.center,
          height: 36,
          // height: 40,
          child: TextField(
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                  fontSize: 14,
                  textBaseline: TextBaseline.alphabetic,
                  fontWeight: FontWeight.w400,
                ),
            controller: textController,
            enabled: widget.onPress == null,
            autofocus: widget.isAutoFocus,
            inputFormatters: [
              LengthLimitingTextInputFormatter(50),
            ],
            onChanged: (text) => _onChangeTextInput(text),
            textInputAction: TextInputAction.done,
            onSubmitted: (text) => _doneListener(text),
            // controller: controller,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 16, top: 3, bottom: 3),
              hintText: widget.hintText,
              hintStyle: Theme.of(context).textTheme.bodyText2.copyWith(
                    fontSize: 14,
                    color: AppColors.ink400,
                    fontWeight: FontWeight.w400,
                  ),
              border: InputBorder.none,
              suffixIcon: Icon(
                Icons.search,
                color: AppColors.ink500,
                size: 18,
              ),
              /*suffixIcon: _showCloseButton ? IconButton(
                icon: Icon(Icons.close,
                  size: 20,
                  color: AppColors.grey,
                ),
                onPressed: _clearText,
              ) : null,*/
            ),
          ),
        ),
      ),
    );
  }

  void _clearText() {
    textController.text = '';
    deBouncer.value = '';
    setState(() {
      _showCloseButton = false;
    });
  }

  _onChangeTextInput(String text) {
    deBouncer.value = text;
    if (text != null && text.isNotEmpty) {
      if (widget.showCloseButton && !_showCloseButton) {
        setState(() {
          _showCloseButton = true;
        });
      }
    } else {
      if (_showCloseButton) {
        setState(() {
          _showCloseButton = false;
        });
      }
    }
  }
}

class DeBouncer<T> {
  DeBouncer(this.duration, this.onValue);
  final Duration duration;
  void Function(T value) onValue;
  T _value;
  Timer _timer;
  T get value => _value;
  set value(T val) {
    _value = val;
    _timer?.cancel();
    _timer = Timer(duration, () => onValue(_value));
  }
}
