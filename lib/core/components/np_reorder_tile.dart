import 'package:flutter/material.dart';
import 'package:real_estate/assets/images/images.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';

import 'np_divider.dart';

class NPReorderTile extends StatelessWidget {
  NPReorderTile({@required this.child, this.indicator = true});

  final Widget child;
  final bool indicator;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          NPDivider(),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: AppDimens.spaceMedium,
              vertical: AppDimens.spaceSmall,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Center(
                        child: Image.asset(
                          AppUtils.assetImage(Images.icSort),
                          fit: BoxFit.fill,
                          width: 20,
                          height: 20,
                        ),
                      ),
                      SizedBox(width: AppDimens.spaceSmall),
                      Expanded(
                        child: child,
                      ),
                    ],
                  ),
                ),
                indicator
                    ? Icon(
                        Icons.navigate_next,
                        color: AppColors.ink400,
                        size: 20,
                      )
                    : SizedBox(
                        width: 0,
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
