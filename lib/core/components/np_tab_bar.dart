import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:real_estate/theme/theme.dart';

const double _kTabHeight = 46.0;
const double _kTextAndIconTabHeight = 72.0;
const double indicatorWeight = 2.0;
const double maxTabWidth = 120;

class NPTabBar extends StatelessWidget implements PreferredSizeWidget {
  final List<Widget> tabs;

  final TabController controller;

  final bool isScrollable;

  final ValueChanged<int> onTap;

  NPTabBar(
      {Key key,
      @required BuildContext context,
      @required List<String> tabTitles,
      this.controller,
      this.isScrollable = false,
      this.onTap})
      : assert(context != null),
        assert(tabTitles != null && tabTitles.isNotEmpty),
        tabs = _getTabs(tabTitles, context),
        super(key: key);

  @override
  Size get preferredSize {
    for (final Widget item in tabs) {
      if (item is Tab) {
        final Tab tab = item;
        if ((tab.text != null || tab.child != null) && tab.icon != null)
          return Size.fromHeight(_kTextAndIconTabHeight + indicatorWeight);
      }
    }
    return Size.fromHeight(_kTabHeight + indicatorWeight);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: Container(
        width: double.infinity,
        child: TabBar(
          indicatorWeight: indicatorWeight,
          labelStyle: TextStyle(
              color: AppColors.blue500,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          labelPadding: EdgeInsets.only(left: 8, right: 8),
          unselectedLabelColor: AppColors.ink400,
          unselectedLabelStyle: TextStyle(
              color: AppColors.ink400,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          indicatorColor: AppColors.blue500,
          labelColor: AppColors.blue500,
          controller: controller,
          isScrollable: isScrollable,
          tabs: tabs,
          onTap: onTap,
        ),
      ),
    );
  }

  static _getTabs(List<String> tabTitles, BuildContext context) {
    final AutoSizeGroup myGroup = AutoSizeGroup();
    return tabTitles
        .map((tab) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxWidth: maxTabWidth,
                  ),
                  child: Tab(
                    child: AutoSizeText(
                      tab,
                      group: myGroup,
                      minFontSize: AppDimens.textSizeXSmall,
                      maxFontSize: AppDimens.textSizeMedium,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )),
            ))
        .toList();
  }
}
