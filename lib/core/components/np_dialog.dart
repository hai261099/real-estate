import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import '../../theme/app_colors.dart';
import '../../theme/app_dimens.dart';
import '../../theme/app_text_styles.dart';
import '../../theme/theme.dart';
import '../../utils/app_utils.dart';
import 'np_button.dart';

enum DialogType { normal, progress, image }

enum DialogTextType { red, primary }

enum ImageSize {
  filled,
  center,
}

class NPDialog {
  final BuildContext context;
  final DialogType type;
  final List<NPActionDialog> actions;
  final String title;
  final String message;
  final bool hasClose;
  final bool barrierDismissible;
  final Image image;
  final ImageSize imageSize;

  NPDialog({
    @required this.context,
    this.type,
    this.actions,
    this.title,
    this.message,
    this.hasClose = false,
    this.image,
    this.imageSize = ImageSize.center,
    this.barrierDismissible = true,
  });

  show() {
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (context) => Center(
              child: Container(
                decoration: BoxDecoration(
                  // border: Border.all(width: 1, color: AppColors.ink300),
                  color: AppColors.white500,
                  borderRadius: new BorderRadius.all(
                      Radius.circular(AppDimens.radiusMedium)),
                ),
                width: 296,
                child: _buildContentDialog(),
              ),
            ));
  }

  Widget _buildContentDialog() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildImageDialog(),
        Container(
          padding: EdgeInsets.only(
              top: AppDimens.spaceMedium,
              left: AppDimens.spaceMedium,
              right: AppDimens.spaceMedium,
              bottom: AppDimens.spaceXSmall8),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // Title
              Text(
                this.title,
                style: AppTextStyles.medium(context,
                    size: 20, lineHeight: AppDimens.lineHeightXLarge),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: AppDimens.spaceXSmall8),

              // Message
              Text(
                this.message,
                style: AppTextStyles.regularSmall(context,
                    color: AppColors.ink400),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: AppDimens.spaceMedium),

              // Actions
              Column(children: this.actions),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildImageDialog() {
    return Container();
  }

  Widget _buildCloseButton() {
    if (hasClose) {
      Positioned(
          top: 0,
          left: 0,
          child: SizedBox(
            width: AppDimens.heightIconSize,
            height: AppDimens.heightIconSize,
            child: RawMaterialButton(
              elevation: 0,
              child: Icon(Icons.close,
                  color: AppColors.red500, size: AppDimens.heightIconSize),
              onPressed: () {
                Navigator.of(context).pop();
              },
              highlightColor: AppColors.white200,
              splashColor: AppColors.ink100,
              // fillColor: AppColors.ink200,
            ),
          ));
    }
    return Container();
  }
}

class NPActionDialog extends StatelessWidget {
  final String title;
  final Function() onPressed;
  final ButtonType type;
  final Color titleColor;

  NPActionDialog(
      {@required this.title,
      this.type = ButtonType.primary,
      this.titleColor,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return NPButton(
      title: this.title,
      titleColor: this.titleColor,
      type: this.type,
      onPressed: () {
        Navigator.of(context).pop();
        if (this.onPressed != null) {
          this.onPressed();
        }
      },
    );
  }
}

class FnbSnackBar {
  final String message;

  FnbSnackBar({@required this.message});

  show(BuildContext context) {
    Flushbar(
      onTap: (e) {
        e.dismiss();
      },
      message: message,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      animationDuration: Duration(milliseconds: 500),
      backgroundColor: AppColors.green500,
      margin: EdgeInsets.only(
        top: AppDimens.spaceMedium * 4,
        left: AppDimens.spaceMedium,
        right: AppDimens.spaceMedium,
      ),
      padding: EdgeInsets.all(
        AppDimens.spaceMedium,
      ),
      isDismissible: false,
      borderRadius: AppDimens.radiusSmall,
      icon: Icon(
        Icons.check_circle,
        color: AppColors.white500,
      ),
      mainButton: Icon(
        Icons.clear,
        color: AppColors.black300,
      ),
    )..show(context);
  }
}
