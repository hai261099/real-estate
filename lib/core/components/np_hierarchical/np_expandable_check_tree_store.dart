import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'np_expandable_check_tree_store.g.dart';

class NPExpandableCheckTreeStore = _NPExpandableCheckTreeStore
    with _$NPExpandableCheckTreeStore;

abstract class _NPExpandableCheckTreeStore with Store {
  static const _CFG_KEY_VALUE_ = 'value';
  static const _CFG_KEY_HIERARCHICAL_ = '_hierarchical';

  // default
  var _itemsCfgKey = 'items';

  @observable
  List<dynamic> nodeList = [];

  String get itemsCfgKey => _itemsCfgKey;

  @action
  Future<void> setupNodes(List<dynamic> nodes, {@required itemsKey}) async {
    _itemsCfgKey = itemsKey;

    List<dynamic> _nodes = List.from(nodes);
    for (var i = 0; i < _nodes.length; i++) {
      _indexHierarchical(_nodes[i], i);
    }

    nodeList = _nodes;
  }

  @action
  Future<void> updateNode(
    dynamic item, {
    @required int index,
    @required value,
  }) async {
    // print(item);
    List<dynamic> temps = List.from(nodeList);

    List<int> hierarchical = List<int>.from(item[_CFG_KEY_HIERARCHICAL_]);

    dynamic entry;

    List<dynamic> hierarchicalEntries = [];

    if (hierarchical == null || hierarchical.isEmpty) {
      entry = temps[index];
    } else {
      entry = temps[hierarchical[0]];
      hierarchicalEntries.add(entry);

      for (var i = 1; i < hierarchical.length; i++) {
        entry = entry[_itemsCfgKey][hierarchical[i]];
        hierarchicalEntries.add(entry);
      }

      entry = entry[_itemsCfgKey][index];
    }

    // set state for entry & subs
    _setValueOfNode(entry, value);

    // update state tree
    for (var i = hierarchicalEntries.length - 1; i >= 0; i--) {
      hierarchicalEntries[i][_CFG_KEY_VALUE_] =
          _valueOfNode(hierarchicalEntries[i]);
    }

    nodeList = temps;
  }

  void _indexHierarchical(dynamic node, int index, {List<int> hierarchicals}) {
    hierarchicals = hierarchicals ?? [];
    node[_CFG_KEY_HIERARCHICAL_] = hierarchicals;
    List<dynamic> items = node[_itemsCfgKey];

    node[_CFG_KEY_VALUE_] = false;

    if (items != null) {
      List<int> subHierarchicals = List<int>.from(hierarchicals);
      subHierarchicals.add(index);

      for (var i = 0; i < items.length; i++) {
        _indexHierarchical(items[i], i, hierarchicals: subHierarchicals);
      }

      node[_itemsCfgKey] = items;
    }
  }

  bool _valueOfNode(dynamic node) {
    List<dynamic> items = node[_itemsCfgKey];

    bool value = true;
    for (var item in items) {
      if (false == item[_CFG_KEY_VALUE_]) {
        value = false;
        break;
      }
    }
    return value;
  }

  void _setValueOfNode(dynamic node, bool value) {
    node[_CFG_KEY_VALUE_] = value;
    List<dynamic> items = node[_itemsCfgKey];

    if (items != null) {
      items.forEach(
        (element) {
          _setValueOfNode(element, value);
        },
      );
    }
  }
}
