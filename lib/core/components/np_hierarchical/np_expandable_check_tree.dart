import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:real_estate/theme/theme.dart';

import '../np_divider.dart';
import '../np_expansion_tile.dart';
import 'np_expandable_check_tree_store.dart';

typedef IndexedTreeWidgetBuilder = Widget Function(
    BuildContext context, int level, int index, dynamic node);

typedef IndexedTreeColorWidgetBuilder = Color Function(
    BuildContext context, int level, int index, dynamic node);

typedef IndexedTreePaddingBuilder = EdgeInsetsGeometry Function(
    BuildContext context, int level, int index, dynamic node);

class NPExpandableCheckTree extends StatelessWidget {
  NPExpandableCheckTree(
    this.nodes, {
    this.onChanged,
    this.itemsKey,
    @required this.titleBuilder,
    this.dividerBuilder,
    this.trailingBuilder,
    this.backgroundBuilder,
    this.paddingBuilder,
  });

  final List<dynamic> nodes;
  final String itemsKey;
  final ValueChanged<dynamic> onChanged;
  final IndexedTreeWidgetBuilder titleBuilder;
  final IndexedTreeWidgetBuilder dividerBuilder;
  final IndexedTreeWidgetBuilder trailingBuilder;
  final IndexedTreeColorWidgetBuilder backgroundBuilder;
  final IndexedTreePaddingBuilder paddingBuilder;
  final NPExpandableCheckTreeStore _store = NPExpandableCheckTreeStore();

  @override
  Widget build(BuildContext context) {
    _store.setupNodes(nodes, itemsKey: itemsKey);

    return Observer(builder: (_) {
      List<dynamic> nodes = _store.nodeList;

      WidgetsBinding.instance.addPostFrameCallback((_) => onChanged(nodes));

      return ListView.separated(
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) => NPCheckNode(
                item: nodes[index],
                itemsKey: _store.itemsCfgKey,
                level: 0,
                index: index,
                onChanged: (node) {
                  _store.updateNode(
                    node.item,
                    index: node.index,
                    value: node.value,
                  );
                },
                titleBuilder: titleBuilder,
                separatorBuilder: dividerBuilder,
                backgroundColorBuilder: backgroundBuilder,
                trailingBuilder: trailingBuilder,
                paddingBuilder: paddingBuilder,
              ),
          separatorBuilder: (context, index) => NPDivider(
                thickness: 1,
              ),
          itemCount: nodes.length);
    });
  }
}

class NPNodeValue {
  NPNodeValue({
    @required this.item,
    @required this.value,
    @required this.index,
  });
  dynamic item;
  bool value;
  int index;
}

class NPCheckNode extends StatelessWidget {
  NPCheckNode(
      {@required this.item,
      @required this.itemsKey,
      @required this.level,
      @required this.index,
      @required this.titleBuilder,
      @required this.onChanged,
      this.separatorBuilder,
      this.backgroundColorBuilder,
      this.trailingBuilder,
      this.paddingBuilder});
  final dynamic item;
  final int level;
  final int index;
  final String itemsKey;

  final ValueChanged<NPNodeValue> onChanged;
  final IndexedTreeWidgetBuilder titleBuilder;
  final IndexedTreeWidgetBuilder separatorBuilder;
  final IndexedTreeWidgetBuilder trailingBuilder;
  final IndexedTreeColorWidgetBuilder backgroundColorBuilder;
  final IndexedTreePaddingBuilder paddingBuilder;

  @override
  Widget build(BuildContext context) {
    return _buildEntry(context, node: item, nodeLevel: level, index: index);
  }

  Widget _buildEntry(BuildContext context,
      {dynamic node, int nodeLevel, int index}) {
    List<dynamic> chidlNodes = node[itemsKey];
    Widget title = titleBuilder(context, nodeLevel, index, node);
    Widget separator = (separatorBuilder != null)
        ? separatorBuilder(context, nodeLevel, index, node)
        : null;
    Color backgroundColor = (backgroundColorBuilder != null)
        ? backgroundColorBuilder(context, nodeLevel, index, node)
        : Colors.transparent;

    Widget trailing = (trailingBuilder != null)
        ? trailingBuilder(context, nodeLevel, index, node)
        : null;

    EdgeInsetsGeometry padding = (paddingBuilder != null)
        ? paddingBuilder(context, nodeLevel, index, node)
        : EdgeInsets.zero;

    List<Widget> entries = [];
    if (chidlNodes != null && chidlNodes.isNotEmpty) {
      entries = [];
      entries.add(
        ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, index) => _buildEntry(context,
                node: chidlNodes[index],
                nodeLevel: nodeLevel + 1,
                index: index),
            separatorBuilder: (context, index) => Padding(
                  padding: EdgeInsets.only(
                    left: (nodeLevel + 1) * AppDimens.spaceMedium,
                  ),
                  child: separator ?? NPDivider(),
                ),
            itemCount: chidlNodes.length),
      );
    }

    return NPExpansionTile(
      title: title,
      trailing: trailing,
      padding: padding,
      tilePadding: EdgeInsets.all(0),
      backgroundColor: backgroundColor,
      initiallyExpanded: true,
      leading: Padding(
        padding: EdgeInsets.only(left: nodeLevel * AppDimens.spaceMedium),
        child: Checkbox(
          activeColor: AppColors.blue500,
          value: node['value'] ?? false,
          onChanged: (e) {
            if (onChanged != null) {
              onChanged(
                NPNodeValue(
                  item: node,
                  value: e,
                  index: index,
                ),
              );
            }
          },
        ),
      ),
      children: entries,
    );
  }
}
