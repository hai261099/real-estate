// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'np_expandable_check_tree_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NPExpandableCheckTreeStore on _NPExpandableCheckTreeStore, Store {
  final _$nodeListAtom = Atom(name: '_NPExpandableCheckTreeStore.nodeList');

  @override
  List<dynamic> get nodeList {
    _$nodeListAtom.reportRead();
    return super.nodeList;
  }

  @override
  set nodeList(List<dynamic> value) {
    _$nodeListAtom.reportWrite(value, super.nodeList, () {
      super.nodeList = value;
    });
  }

  final _$setupNodesAsyncAction =
      AsyncAction('_NPExpandableCheckTreeStore.setupNodes');

  @override
  Future<void> setupNodes(List<dynamic> nodes, {dynamic itemsKey}) {
    return _$setupNodesAsyncAction
        .run(() => super.setupNodes(nodes, itemsKey: itemsKey));
  }

  final _$updateNodeAsyncAction =
      AsyncAction('_NPExpandableCheckTreeStore.updateNode');

  @override
  Future<void> updateNode(dynamic item, {int index, dynamic value}) {
    return _$updateNodeAsyncAction
        .run(() => super.updateNode(item, index: index, value: value));
  }

  @override
  String toString() {
    return '''
nodeList: ${nodeList}
    ''';
  }
}
