import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'np_textfield.dart';

TextEditingValue _formatedValue(String input, {String locale}) {
  if (input == null || input.trim().isEmpty) {
    return TextEditingValue();
  }
  // final f = NumberFormat.decimalPattern(Localizations.localeOf(context).languageCode);
  input = input.trim().replaceAll('.', '').replaceAll(',', '');

  final f = NumberFormat.decimalPattern(locale ?? 'en');

  final valueChanged = int.parse(input);
  final String formatedText = f.format(valueChanged);

  TextEditingValue _value = TextEditingValue(
      text: formatedText,
      selection: TextSelection.collapsed(offset: formatedText.length));

  return _value;
}

class FnBNumberInputEditingController extends TextEditingController {
  String _locale;
  @override
  set text(String newText) {
    value = _formatedValue(newText, locale: _locale);
  }

  set locale(String lc) {
    _locale = lc;
  }
}

class FnBNumberInputField extends StatefulWidget {
  final Function(String) onChangedText;
  final String labelText;
  final String hintText;
  final bool labelAlways;
  final String locale;
  final FnBNumberInputEditingController controller;

  FnBNumberInputField(
      {this.labelText,
      this.onChangedText,
      this.hintText,
      this.controller,
      this.labelAlways = true,
      this.locale});

  @override
  _FnBNumberInputFieldState createState() => _FnBNumberInputFieldState();
}

class _FnBNumberInputFieldState extends State<FnBNumberInputField> {
  var _hintTextFormated;
  _onChange(String character, BuildContext context) {
    final valueChanged = int.parse(character);
    widget.controller.value = _formatedValue(character);

    if (widget.onChangedText != null) {
      widget.onChangedText(valueChanged.toString());
    }
  }

  set text(String newText) {
    widget.controller.value = _formatedValue(newText);
  }

  @override
  Widget build(BuildContext context) {
    _hintTextFormated = _formatedValue(widget.hintText).text;

    widget.controller.locale =
        widget.locale ?? Localizations.localeOf(context).languageCode;

    return NPTextField(
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      onChangedText: (String character) => {_onChange(character, context)},
      labelText: widget.labelText ?? '',
      controller: widget.controller,
      labelAlways: widget.labelAlways,
      hintText: _hintTextFormated,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      // maxLength: 12,
    );
  }
}
