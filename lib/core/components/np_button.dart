import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';

class NPButton extends StatefulWidget {
  final String title;
  final String subtitle;
  final Widget icon;
  final Function onPressed;
  final isDisable;
  final ButtonType type;
  final ButtonSize size;
  final Color backgroundColor;
  final Color titleColor;
  final double elevation;

  NPButton(
      {this.title = '',
      this.subtitle = '',
      this.icon,
      this.size = ButtonSize.normal,
      this.onPressed,
      this.isDisable = false,
      this.type = ButtonType.primary,
      this.backgroundColor,
      this.titleColor,
      this.elevation});

  @override
  _NPButtonState createState() => _NPButtonState();
}

class _NPButtonState extends State<NPButton> {
  @override
  Widget build(BuildContext context) {
    double spaceItem = widget.icon != null ? 8 : 0;
    var bgColor = widget.backgroundColor ?? backgroundColorByType();
    var height = sizeButtonByType();

    return Container(
      height: height,
      child: RaisedButton(
        onPressed: widget.onPressed,
        elevation: 0,
        color: bgColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildIcon(),
            SizedBox(width: spaceItem),
            _buildText()
          ],
        ),
      ),
    );
  }

  Widget _buildIcon() {
    if (widget.icon != null) {
      return Container(
          width: AppDimens.heightIconSize,
          height: AppDimens.heightIconSize,
          child: widget.icon);
    }
    return Container();
  }

  Widget _buildText() {
    List<Widget> children = [];
    if (widget.title != null && widget.title.length > 0) {
      var textColor = widget.titleColor ?? textColorByType();
      children.add(Text(
        widget.title,
        style: AppTextStyles.mediumMedium(context, color: textColor),
      ));
    }

    if (widget.subtitle != null && widget.subtitle.length > 0) {
      var textColor = widget.titleColor ?? textColorByType();
      children.add(Text(
        widget.subtitle,
        style: AppTextStyles.mediumXSmall(context, color: textColor),
      ));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: children,
    );
  }

  Color backgroundColorByType() {
    var result;
    switch (widget.type) {
      case ButtonType.primary:
        result = AppColors.primaryA500;
        break;
      case ButtonType.primaryDestruction:
        result = AppColors.red500;
        break;
      case ButtonType.primaryNotice:
        result = AppColors.green500;
        break;
      case ButtonType.disabled:
        result = AppColors.ink100;
        break;
      case ButtonType.secondary:
        result = Colors.transparent;
        break;
      case ButtonType.secondaryDestruction:
        result = AppColors.red200;
        break;
      default:
        result = AppColors.primaryA500;
    }
    return result;
  }

  Color textColorByType() {
    var result;
    switch (widget.type) {
      case ButtonType.primary:
      case ButtonType.primaryDestruction:
      case ButtonType.primaryNotice:
        result = AppColors.white;
        break;
      case ButtonType.disabled:
        result = AppColors.ink400;
        break;
      case ButtonType.secondary:
        result = AppColors.primaryA500;
        break;
      case ButtonType.secondaryDestruction:
        result = AppColors.red500;
        break;
      default:
        result = AppColors.primaryA500;
    }
    return result;
  }

  double sizeButtonByType() {
    // size Normal
    double result = 0;
    switch (widget.size) {
      case ButtonSize.normal:
        result = AppDimens.buttonSizeNormal;
        break;
      case ButtonSize.small:
        result = AppDimens.buttonSizeSmall;
        break;
      case ButtonSize.compact:
        result = AppDimens.buttonSizeCompact;
        break;
    }
    return result;
  }
}
