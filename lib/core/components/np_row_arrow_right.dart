import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class NPRowArrowRight extends StatelessWidget {
  final String title;
  final String subTitle;
  final Function onPressed;

  NPRowArrowRight({this.title = '', this.subTitle = '', this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (this.onPressed != null) {
          this.onPressed();
        }
      },
      child: Container(
        padding: EdgeInsets.only(
            right: AppDimens.spaceMedium,
            left: AppDimens.spaceMedium,
            top: AppDimens.spaceSmall,
            bottom: AppDimens.spaceSmall),
        child: Row(
          children: [
            Expanded(
                child: Text(this.title,
                    style: AppTextStyles.regularSmall(context))),
            Text(
              this.subTitle,
              style: AppTextStyles.mediumSmall(context),
            ),
            SizedBox(width: AppDimens.spaceMedium),
            Icon(Icons.arrow_forward_ios,
                color: AppColors.ink400, size: AppDimens.heightIconSize / 2)
          ],
        ),
      ),
    );
  }
}
