import 'package:flutter/material.dart';
import 'package:real_estate/assets/images/images.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/app_utils.dart';

class NPStepperIndicator extends StatelessWidget {
  final List<String> items;
  final int currentIndex;
  final Color activeColor;
  final Color disableColor;
  final double lineSpace;
  final Function(int) onPressed;

  NPStepperIndicator(
      {this.items,
      this.currentIndex = 0,
      this.lineSpace = 2.0,
      this.onPressed,
      this.activeColor = AppColors.blue500,
      this.disableColor = AppColors.ink400});

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min, children: _buildStepItems(context));
  }

  List<Widget> _buildStepItems(BuildContext context) {
    var results = List<Widget>();
    int index = 0;
    items.forEach((element) {
      results.add(_buildStepItem(context, index));
      index++;
    });
    return results;
  }

  Widget _buildStepItem(BuildContext context, int index) {
    var sizeStepIndex = AppDimens.heightIconSize;
    var maxItem = items.length;
    var width = (MediaQuery.of(context).size.width -
            maxItem * sizeStepIndex -
            maxItem * lineSpace * 2) /
        ((maxItem + 1) * 2);
    var firstColor = Colors.transparent;
    if (index != 0) {
      if (currentIndex >= index) {
        firstColor = activeColor;
      } else {
        firstColor = disableColor;
      }
    }
    var endColor = Colors.transparent;
    if (index < maxItem - 1) {
      if (index < currentIndex) {
        endColor = activeColor;
      } else {
        endColor = disableColor;
      }
    }

    var textColor = disableColor;
    if (index < currentIndex) {
      textColor = AppColors.ink500;
    } else if (index == currentIndex) {
      textColor = activeColor;
    }

    var bgStepColor = disableColor;
    if (index <= currentIndex) {
      bgStepColor = activeColor;
    }

    return InkWell(
      onTap: () {
        if (onPressed != null) {
          onPressed(index);
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // index step
          Row(
            children: <Widget>[
              Container(
                height: 1,
                color: firstColor,
                width: width,
              ),
              SizedBox(
                width: lineSpace,
              ),
              ClipRRect(
                  borderRadius: BorderRadius.circular(sizeStepIndex / 2),
                  child: Container(
                      child: index < currentIndex
                          ? Image.asset(
                              AppUtils.assetImage(Images.icCheckCircleBlue))
                          : Center(
                              child: Text('${index + 1}',
                                  style: AppTextStyles.mediumXSmall(context,
                                      color: AppColors.white500))),
                      width: sizeStepIndex,
                      height: sizeStepIndex,
                      color: bgStepColor)),
              SizedBox(
                width: lineSpace,
              ),
              Container(
                height: 1,
                color: endColor,
                width: width,
              ),
            ],
          ),
          Text(
            items[index],
            style: AppTextStyles.regularXSmall(context, color: textColor),
          )
        ],
      ),
    );
  }
}
