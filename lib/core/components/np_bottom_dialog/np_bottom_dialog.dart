import 'dart:core';

import 'package:flutter/material.dart';

import 'np_bar_bottom_sheet.dart';
import 'np_material_bottom_sheet.dart';
import 'modal/np_modal_bar.dart';
import 'modal/np_modal_material.dart';

export 'np_bar_bottom_sheet.dart';
export 'np_material_bottom_sheet.dart';

enum BottomDialogType {
  sideBar,
  material,
  custom,
}

class NPBottomDialog {
  final BuildContext context;
  final BottomDialogType type;
  final Widget rightItem;
  final bool hasCloseButton;
  final String titleDialog;
  final List<NPBottomDialogItem> items;
  final bool hasTopSlide;
  final Function(NPBottomDialogItem) didSelectedItem;
  final Widget child;

  NPBottomDialog(
      {@required this.context,
      this.type = BottomDialogType.sideBar,
      this.rightItem,
      this.hasCloseButton = false,
      this.titleDialog = '',
      this.items,
      this.hasTopSlide,
      this.child,
      this.didSelectedItem});

  show() {
    if (this.type == BottomDialogType.material) {
      showMaterialModalBottomSheet(
          context: context,
          expand: false,
          backgroundColor: Colors.transparent,
          builder: (context, scrollController) => NPModalMaterial(
                hasCloseButton: this.hasCloseButton,
                title: this.titleDialog,
                items: this.items,
                didSelectedItem: this.didSelectedItem,
                scrollController: scrollController,
              ));
    } else if (this.type == BottomDialogType.sideBar) {
      showBarModalBottomSheet(
          expand: false,
          hasTopSlide: hasTopSlide,
          context: context,
          backgroundColor: Colors.transparent,
          builder: (context, scrollController) => NPModalBar(
                didSelectedItem: this.didSelectedItem,
                hasCloseButton: this.hasCloseButton,
                title: this.titleDialog,
                items: this.items,
                scrollController: scrollController,
              ));
    } else {
      assert(this.child == null, 'Child bottom dialog custom is not null');
      showMaterialModalBottomSheet(
          context: context,
          expand: false,
          backgroundColor: Colors.transparent,
          builder: (context, scrollController) =>
              Material(child: SafeArea(top: false, child: this.child)));
    }
  }
}

class NPBottomDialogItem {
  final String title;
  final String itemKey;
  final String urlImage;
  final String imagePath;

  NPBottomDialogItem({this.title, this.itemKey, this.urlImage, this.imagePath});
}
