import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

import '../np_bottom_dialog.dart';

class NPModalBar extends StatelessWidget {
  final ScrollController scrollController;
  final bool hasCloseButton;
  final String title;
  final List<NPBottomDialogItem> items;
  final Function(NPBottomDialogItem) didSelectedItem;

  const NPModalBar(
      {Key key,
      this.scrollController,
      this.hasCloseButton,
      this.title,
      this.items,
      this.didSelectedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
          leading: this.hasCloseButton
              ? Container(
                  width: AppDimens.buttonSizeNormal,
                  height: AppDimens.buttonSizeNormal,
                  child: IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Icon(Icons.close),
                      alignment: Alignment.centerLeft,
                      color: Colors.black,
                      iconSize: AppDimens.buttonSizeXSmall,
                      onPressed: () {
                        Navigator.of(context).pop();
                      }))
              : Container(),
          middle: Text(this.title, style: AppTextStyles.mediumMedium(context))),
      child: SafeArea(
        bottom: false,
        child: ListView(
          shrinkWrap: true,
          controller: scrollController,
          physics: BouncingScrollPhysics(),
          children: ListTile.divideTiles(
              context: context,
              tiles: this.items.map((item) => ListTile(
                  title: Text(item.title),
                  leading: _leadingItem(item),
                  onTap: () {
                    if (this.didSelectedItem != null) {
                      this.didSelectedItem(item);
                    }
                    Navigator.of(context).pop();
                  }))).toList(),
        ),
      ),
    ));
  }

  Widget _leadingItem(NPBottomDialogItem item) {
    if (item.urlImage != null || item.imagePath != null) {
      if (item.imagePath != null) {
        return Container(
          width: AppDimens.buttonSizeXSmall,
          height: AppDimens.buttonSizeXSmall,
          child: Image.asset(item.imagePath),
        );
      }

      // Image from internet
      return CachedNetworkImage(
        fit: BoxFit.cover,
        width: AppDimens.buttonSizeXSmall,
        height: AppDimens.buttonSizeXSmall,
        imageUrl: item.urlImage,
        placeholder: (context, url) {
          return Container(
            width: AppDimens.buttonSizeXSmall,
            height: AppDimens.buttonSizeXSmall,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
        // errorWidget: (context, url, objext) {
        //   return _buildDefaultLogoMarker();
        // },
      );
    }
    return null;
  }
}
