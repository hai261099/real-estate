import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class NPDivider extends StatelessWidget {
  final double indent;

  final Color color;

  final double height;

  final double thickness;

  const NPDivider(
      {Key key,
      this.indent = 0,
      this.color = AppColors.divider,
      this.height = 1.5,
      this.thickness = 1.5})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Divider(
        color: color,
        height: height,
        thickness: thickness,
        indent: indent,
      ),
    );
  }
}
