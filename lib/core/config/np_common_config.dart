// import 'dart:convert';

// class CommonConfig {
//   static const String _PREFIX_LANGUAGE = 'np/strings/';
//   static const String _PREFIX_LANGUAGE_ERROR_CODE = 'np/strings/errorCode/';

//   static Future<Map<String, dynamic>> getLanguageResourceFromRemote(
//       String languageCode) async {
//     MAFDataCenter dataCenter = await MAFDataCenter.getInstance();
//     if (dataCenter.get(_PREFIX_LANGUAGE + languageCode) == null) {
//       return {};
//     }
//     dynamic data = jsonDecode(dataCenter.get(_PREFIX_LANGUAGE + languageCode));
//     if (data == null) {
//       return {};
//     }
//     return data;
//   }

//   static Future<void> saveRemoteLanguageConfig(
//       Map<String, dynamic> json) async {
//     if (json.length > 0) {
//       MAFDataCenter dataCenter = await MAFDataCenter.getInstance();
//       json.forEach((key, value) {
//         dataCenter.set(_PREFIX_LANGUAGE + key, jsonEncode(value));
//       });
//       await dataCenter.saveToStorage();
//     }
//   }

//   static Future<Map<String, dynamic>> getLanguageErrorCodeResourceFromRemote(
//       String languageCode) async {
//     MAFDataCenter dataCenter = await MAFDataCenter.getInstance();
//     if (dataCenter.get(_PREFIX_LANGUAGE_ERROR_CODE + languageCode) == null) {
//       return {};
//     }
//     dynamic data =
//         jsonDecode(dataCenter.get(_PREFIX_LANGUAGE_ERROR_CODE + languageCode));
//     if (data == null) {
//       return {};
//     }
//     return data;
//   }

//   static Future<void> saveRemoteLanguageErrorCodeConfig(
//       Map<String, dynamic> json) async {
//     if (json.length > 0) {
//       MAFDataCenter dataCenter = await MAFDataCenter.getInstance();
//       json.forEach((key, value) {
//         dataCenter.set(_PREFIX_LANGUAGE_ERROR_CODE + key, jsonEncode(value));
//       });
//       await dataCenter.saveToStorage();
//     }
//   }
// }
