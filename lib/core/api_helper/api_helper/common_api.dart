import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:real_estate/core/api_helper/api_base/api_client.dart';
import 'package:real_estate/core/api_helper/api_base/api_response.dart';
import 'package:real_estate/core/api_helper/endpoint_data.dart';
import 'package:real_estate/utils/app_utils.dart';

class CommonApi {
  static Future<ApiResponse> getRemoteLanguage() async {
    return await ApiClient().request(
      endpointKey: EndPointKey.RemoteLanguageAPI,
      data: jsonEncode(
        {
          ParamAPIKey.CHANNEL: 'APP',
        },
      ),
    );
  }

  static Future<ApiResponse> getRemoteErrorLanguage() async {
    return await ApiClient().request(
      endpointKey: EndPointKey.RemoteLanguageErrorCodeAPI,
      data: jsonEncode(
        {
          ParamAPIKey.CHANNEL: 'APP',
        },
      ),
    );
  }

  static Future<ApiResponse> getSystemConfig() async {
    return await ApiClient().request(
      endpointKey: EndPointKey.RemoteSystemConfig,
      data: jsonEncode(
        {
          ParamAPIKey.CHANNEL: 'APP',
        },
      ),
    );
  }

  static Future<ApiResponse> uploadFile(List<File> files) async {
    Map<String, dynamic> formData = {
      ParamAPIKey.FOLDER: Const.FOLDER_IMAGES_NAME,
    };
    if (files.length == 1) {
      formData[ParamAPIKey.FILE_NAME] =
          AppUtils.genarateFileNameTimeByPath(files[0].path);
      formData[ParamAPIKey.FILE] = await MultipartFile.fromFile(files[0].path);
    } else if (files.length > 1) {
      List<MultipartFile> listMultipart = [];
      for (int i = 0; i <= files.length; i++) {
        listMultipart.add(await MultipartFile.fromFile(files[i].path));
      }
      formData[ParamAPIKey.FILE] = listMultipart;
      formData[ParamAPIKey.FILE_NAME] =
          AppUtils.genarateFileNameTimeByPath(files[0].path);
    }

    return await ApiClient()
        .request(url: URLData.urlUploadImages, formData: formData);
  }
}
