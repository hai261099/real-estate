// Define Parametar below

import 'package:real_estate/app/app_config.dart';

class ParamAPIKey {
  static const CHANNEL = 'channel';
  static const ID = 'id';
  static const FOLDER = 'folder';
  static const FILE_NAME = 'fileName';
  static const FILE = 'file';
}

// Define EndPointKey below
class EndPointKey {
  static const RemoteLanguageAPI = 'RemoteLanguageAPI';
  static const RemoteSystemConfig = 'ntp.cfg.supper-config';
  static const RemoteLanguageErrorCodeAPI = 'RemoteLanguageErrorCodeAPI';
}

class URLData {
  static const urlUploadImages = 'https://api-fnb.nextshop.dev/s3/image';
}

class EndpointData {
  Map<String, String> _data;
  static const Map<String, String> _mockData = {
    EndPointKey.RemoteLanguageAPI: 'nextpay-fnb-account/cfg/language',
    EndPointKey.RemoteLanguageErrorCodeAPI:
        'nextpay-fnb-account/cfg/language-error-code',
    EndPointKey.RemoteSystemConfig: 'nextpay-fnb-account/cfg/fnb-config'
  };

  static EndpointData _instance;

  EndpointData._create();

  static Future<EndpointData> getInstance() async {
    if (_instance == null) {
      _instance = EndpointData._create();
      _instance._data = new Map<String, String>();
      _instance._data.addAll(_mockData);
    }
    return _instance;
  }

  cleanInstance() {
    _instance = null;
  }

  String get(String key) {
    return AppConfig.instance.values.baseApiHost + _data[key];
  }
}
