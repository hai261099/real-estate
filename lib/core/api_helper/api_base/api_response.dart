import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

part 'api_response.g.dart';

@JsonSerializable(nullable: false)
class ApiResponse {
  String code;
  dynamic data;
  String message;
  bool success;
  int totalItem;
  int totalItemRespondent;

  @JsonKey(ignore: true)
  Response dioResponse;

  ApiResponse(
      {this.code,
      this.data,
      this.message,
      this.success,
      this.totalItem,
      this.totalItemRespondent});

  factory ApiResponse.fromJson(Map<String, dynamic> json) =>
      _$ApiResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ApiResponseToJson(this);

  setData({dynamic data}) {
    this.data = data;
  }

  setMessage({String message}) {
    this.message = message;
  }
}
