import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:real_estate/app/app_config.dart';
import 'package:real_estate/utils/app_utils.dart';

import '../endpoint_data.dart';
import 'api_response.dart';

class ApiClient {
  static const String GET = 'GET';
  static const String POST = 'POST';
  static const String DELETE = 'DELETE';
  static const String PATCH = 'PATCH';
  static const String PUT = 'PUT';

  static const CONTENT_TYPE = 'Content-Type';
  static const CONTENT_TYPE_JSON = 'application/json; charset=utf-8';

  static final BaseOptions defaultOptions = BaseOptions(
      connectTimeout: 10000,
      receiveTimeout: 5000,
      responseType: ResponseType.json);

  Dio _dio;

  static final Map<BaseOptions, ApiClient> _instanceMap = {};

  factory ApiClient({BaseOptions options}) {
    if (options == null) options = defaultOptions;
    if (_instanceMap.containsKey(options)) {
      return _instanceMap[options];
    }
    final ApiClient apiClient = ApiClient._create(options: options);
    _instanceMap[options] = apiClient;
    return apiClient;
  }

  ApiClient._create({BaseOptions options}) {
    if (options == null) options = defaultOptions;
    _dio = Dio(options);
    if (AppConfig.instance.values.log) {
      _dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 180));
    }
  }

  _requestHeader() async {
    String token = AppDataCenter.getToken();
    String time = AppDataCenter.getTime();
    Map<String, dynamic> headerMap =
        token != null ? {'token': token} : new Map();

    headerMap.putIfAbsent('time', () => '$time');
    headerMap.putIfAbsent('accept', () => '*/*');
    headerMap.putIfAbsent('$CONTENT_TYPE', () => '$CONTENT_TYPE_JSON');

    return headerMap;
  }

  static ApiClient get instance => ApiClient();

  Future<ApiResponse> request(
      {String url,
      String endpointKey,
      String method = POST,
      String data,
      Map<String, dynamic> formData,
      Map<String, dynamic> queryParameters,
      bool getFullResponse = false}) async {
    if (endpointKey != null) {
      var endpoint = await EndpointData.getInstance();
      url = endpoint.get(endpointKey);
    }
    Map<String, dynamic> headerMap = await _requestHeader();
    Response response;
    try {
      response = await _dio.request(url,
          data: formData != null
              ? FormData.fromMap(formData)
              : data ?? jsonEncode({}),
          options: Options(
              method: method,
              sendTimeout: 10000,
              receiveTimeout: 10000,
              headers: headerMap,
              contentType: formData != null ? 'multipart/form-data' : null),
          queryParameters: queryParameters);
      if (_isSuccessful(response.statusCode)) {
        var apiResponse = ApiResponse.fromJson(response.data);
        if (getFullResponse) apiResponse.dioResponse = response;
        return apiResponse;
      }
    } on DioError catch (e) {
      if (e.response != null) {
        return ApiResponse(
          success: false,
          data: null,
          message: e.response.statusMessage,
          code: e.response.statusCode.toString(),
        );
      }
      if (e.error is SocketException) {
        SocketException socketException = e.error as SocketException;
        return ApiResponse(
          success: false,
          data: null,
          message: socketException.osError != null
              ? socketException.osError.message
              : '',
          code: socketException.osError != null
              ? socketException.osError.errorCode.toString()
              : '0',
        );
      }
      return ApiResponse(
        success: false,
        data: null,
        message: e.error != null ? e.error : '',
        code: '-9999',
      );
    }
    throw ('Request NOT successful');
  }

  Future<ApiResponse> uploadFile(
      {String url,
      String endpointKey,
      FormData data,
      Map<String, dynamic> queryParameters,
      bool getFullResponse = false}) async {
    if (endpointKey != null) {
      var endpoint = await EndpointData.getInstance();
      url = endpoint.get(endpointKey);
    }

    String token = await AppDataCenter.getToken();
    Map<String, dynamic> headerMap =
        token != null ? {'Authorization': token} : null;

    Response response = await _dio.request(url,
        data: data,
        options: Options(method: POST, headers: headerMap),
        queryParameters: queryParameters);
    if (_isSuccessful(response.statusCode)) {
      var apiResponse = ApiResponse.fromJson(response.data);
      if (getFullResponse) apiResponse.dioResponse = response;
      return apiResponse;
    }
    throw ('Request NOT successful :[code=${response.statusCode}, statusMessage=${response.statusMessage}');
  }

  bool _isSuccessful(int i) {
    return i >= 200 && i <= 299;
  }
}
