import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class TextInput extends StatelessWidget {
  const TextInput({Key key, this.labelText, this.icon, this.obscure})
      : super(key: key);

  final String labelText;
  final IconData icon;
  final bool obscure;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscure ?? false,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        labelText: labelText,
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: AppColors.mediumPurple, width: 2)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: AppColors.mediumPurple, width: 2)),
      ),
    );
  }
}
