import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/screens/about/about.dart';
import 'package:real_estate/screens/faq/faq.dart';
import 'package:real_estate/screens/login/login.dart';
import 'package:real_estate/theme/theme.dart';

class DrawerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];

    List<IconData> _iconOptions = [
      MaterialCommunityIcons.help_circle,
      MaterialCommunityIcons.comment_alert,
      MaterialCommunityIcons.information,
      Icons.logout
    ];

    List<String> _labelOptions = ['F.A.Q', 'Feedback', 'About Us', 'Log out'];

    _onSelectedItem(index) {
      Navigator.of(context).pop();

      switch (index) {
        case 0:
          Navigator.of(context).pushNamed(FaqScreen.routeName);
          break;
        case 1:
          break;
        case 2:
          Navigator.of(context).pushNamed(AboutScreen.routeName);
          break;
        case 3:
          Navigator.of(context)
              .pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
          break;
        default:
          return Text('Error');
          break;
      }
    }

    for (int i = 0; i < _iconOptions.length; i++) {
      drawerOptions.add(ListTile(
          onTap: () => _onSelectedItem(i),
          leading: Icon(_iconOptions[i], color: AppColors.mediumPurple),
          title: Text(_labelOptions[i])));
    }

    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(minRadius: 15, maxRadius: 35),
                    Text('Real Estate App',
                        style: AppTextStyles.mediumLarge(context,
                            color: AppColors.white)),
                    Text('haind1@nextpay.vn',
                        style: AppTextStyles.regularSmall(context,
                            color: AppColors.white))
                  ]),
              decoration: BoxDecoration(color: AppColors.purpleBlueviolet)),
          Padding(
              padding: EdgeInsets.only(left: 25.0),
              child: Column(children: drawerOptions))
        ],
      ),
    );
  }
}
