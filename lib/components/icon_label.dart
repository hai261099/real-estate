import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class IconAndLabel extends StatelessWidget {
  const IconAndLabel({Key key, this.icon, this.label}) : super(key: key);

  final String label;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(children: [
      Icon(icon, color: AppColors.white),
      Text(label,
          style: AppTextStyles.regularSmall(context, color: AppColors.white))
    ]));
  }
}
