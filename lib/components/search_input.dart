import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class SearchInput extends StatelessWidget {
  const SearchInput({
    Key key,
    this.hintText,
    @required this.searchController,
  }) : super(key: key);

  final TextEditingController searchController;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: searchController,
      decoration: InputDecoration(
          fillColor: AppColors.white,
          filled: true,
          prefixIcon: Icon(Icons.search, color: AppColors.primaryA400),
          hintText: hintText,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide.none)),
    );
  }
}
