import 'package:flutter/material.dart';

class BuildImageContainer extends StatelessWidget {
  const BuildImageContainer({Key key, this.image}) : super(key: key);
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width / 2,
        height: MediaQuery.of(context).size.width / 2,
        child: Image.asset(image));
  }
}
