import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:real_estate/app/app_config.dart';

import 'app/app.dart';

void main() {
  //debug config
  AppConfig(config: AppConfigValues.DEV);
  WidgetsFlutterBinding.ensureInitialized();

  runApp(Application());
}
