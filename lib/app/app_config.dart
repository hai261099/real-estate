import 'package:flutter/foundation.dart';

class AppConfigValues {
  final String name;

  final bool log;
  final bool mock;
  final String versionUrl;
  final String baseApiHost;

  const AppConfigValues({
    @required this.name,
    @required this.log,
    @required this.mock,
    @required this.versionUrl,
    @required this.baseApiHost,
  });

  static const AppConfigValues DEV = AppConfigValues(
    name: 'DEV',
    log: true,
    mock: true,
    versionUrl: '',
    baseApiHost: 'https://api-fnb.nextshop.dev/',
  );

  static const AppConfigValues STAGING = AppConfigValues(
    name: 'STAGING',
    log: true,
    mock: true,
    versionUrl: '',
    baseApiHost: 'https://fnb-api.nextshop.app/',
  );

  static const AppConfigValues TEST = AppConfigValues(
    name: 'TEST',
    log: true,
    mock: false,
    versionUrl:
        'http://dev.fastgo.mobi:9868/public/json/chqa-version-config-test.json',
    baseApiHost: 'http://app.test.fastgo.cloud:7222',
  );

  static const AppConfigValues PRODUCTION = AppConfigValues(
    name: 'PRODUCTION',
    log: false,
    mock: false,
    versionUrl: 'https://fastgo.mobi/public/json/chqa-version-config.json',
    baseApiHost: 'https://fastgo.mobi/api',
  );
}

class AppConfig {
  final AppConfigValues values;

  final bool isSubApp;

  static AppConfig _instance;

  static AppConfig get instance {
    return _instance;
  }

  factory AppConfig({@required AppConfigValues config, bool isSubApp = false}) {
    _instance ??= AppConfig._create(config, isSubApp);
    return _instance;
  }

  AppConfig._create(this.values, this.isSubApp);
}
