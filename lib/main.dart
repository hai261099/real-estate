import 'package:flutter/material.dart';
import 'package:real_estate/routes/routes.dart';
import 'package:real_estate/screens/introduction/introduction.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        routes: routes,
        initialRoute: Introduction.routeName);
  }
}
