import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:real_estate/app/app_config.dart';
import 'package:real_estate/core/config/np_common_config.dart';
import 'package:real_estate/localization/text_names.dart';
import 'package:real_estate/utils/app_const.dart';

class NPLocalizations {
  final Locale locale;
  Map<String, dynamic> _defaultSentences;
  Map<String, dynamic> _mainSentences = HashMap();

  Map<String, dynamic> _defaultErrorCodeSentences;
  Map<String, dynamic> _mainErrorCodeSentences = HashMap();

  final bool isTest;

  NPLocalizations(this.locale, {this.isTest = false});

  static NPLocalizations of(BuildContext context) {
    return Localizations.of<NPLocalizations>(context, NPLocalizations);
  }

  Future<NPLocalizations> loadTest(Locale locale) async {
    return NPLocalizations(locale);
  }

  /// Load json files and decode them
  Future<NPLocalizations> load() async {
    String defaultLanguage = await rootBundle.loadString(
        (AppConfig.instance.isSubApp ? Const.PACKAGE_APP_PATH : '') +
            'lib/assets/strings/${locale.languageCode}.json');
    this._defaultSentences = json.decode(defaultLanguage);

    String defaultErrorCodeLanguage = await rootBundle.loadString(
        (AppConfig.instance.isSubApp ? Const.PACKAGE_APP_PATH : '') +
            'lib/assets/strings/errorCode/${locale.languageCode}.json');
    this._defaultErrorCodeSentences = json.decode(defaultErrorCodeLanguage);

    // this._mainSentences =
    //     await CommonConfig.getLanguageResourceFromRemote(locale.languageCode);
    // this._mainErrorCodeSentences =
    //     await CommonConfig.getLanguageErrorCodeResourceFromRemote(
    //         locale.languageCode);

    return NPLocalizations(locale);
  }

  String getStringLabel(BuildContext context, String key) {
    if (isTest) {
      return key;
    }

    if (key == null || key.isEmpty) {
      return key;
    }
    String ret = _mainSentences[key] ?? _defaultSentences[key] ?? key;
    return ret;
  }

  String getStringError(String key) {
    if (isTest) {
      return key;
    }

    if (key == null || key.isEmpty) {
      return _mainSentences[TextName.errorUnknown] ??
          _defaultSentences[TextName.errorUnknown] ??
          TextName.errorUnknown;
    }

    String ret =
        _mainErrorCodeSentences[key] ?? _defaultErrorCodeSentences[key] ?? key;
    return ret;
  }
}
