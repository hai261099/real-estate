import 'package:flutter/material.dart';
import 'package:real_estate/components/text_form_field.dart';
import 'package:real_estate/screens/bottom_bar/bottom_bar.dart';
import 'package:real_estate/theme/theme.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(children: [
      BuildTextFormField(
          icon: Icons.email,
          type: TextInputType.emailAddress,
          labelText: 'Email',
          obscure: false),
      BuildTextFormField(
          icon: Icons.remove_red_eye,
          labelText: 'Password',
          obscure: true,
          type: TextInputType.text),
      SizedBox(height: 20),
      Container(
          height: 45,
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: AppColors.primaryA400),
              onPressed: () {
                Navigator.of(context).pushNamed(BottomBar.routeName);
              },
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text('LOGIN',
                    style: AppTextStyles.regularMedium(context,
                        color: AppColors.white)),
                SizedBox(width: 5),
                Icon(Icons.login)
              ])))
    ]));
  }
}
