import 'package:flutter/material.dart';
import 'package:real_estate/components/image_container.dart';
import 'package:real_estate/screens/forgot_password/forgot_password.dart';
import 'package:real_estate/screens/sign_up/sign_up.dart';
import 'package:real_estate/theme/theme.dart';

import 'login_form.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
          top: 0,
          left: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_1.png')),
      Positioned(
          bottom: 0,
          right: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_2.png')),
      Center(
          child: Container(
              width: MediaQuery.of(context).size.width - 100,
              height: MediaQuery.of(context).size.height / 2,
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(5)),
              child: Column(children: [
                SizedBox(height: 30),
                Text('Real Estate App',
                    style: AppTextStyles.mediumLarge(context)),
                Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: LoginForm()),
                SizedBox(height: 10),
                InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(ForgotPasswordScreen.routeName);
                    },
                    child: Text('Forgot your password?',
                        style: AppTextStyles.regularSmall(context,
                            color: AppColors.primaryA400))),
                Expanded(
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .pushNamed(SignUpScreen.routeName);
                            },
                            child: Text('SIGNUP')))),
                SizedBox(height: 20)
              ])))
    ]);
  }
}
