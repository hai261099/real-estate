import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

import 'components/body.dart';

class LoginScreen extends StatelessWidget {
  static String routeName = 'login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: AppColors.primaryA400, body: Body());
  }
}
