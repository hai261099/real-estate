import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:real_estate/components/search_input.dart';
import 'package:real_estate/theme/theme.dart';

class FaqScreen extends StatelessWidget {
  static String routeName = 'faq';

  @override
  Widget build(BuildContext context) {
    TextEditingController searchController = TextEditingController();

    List<String> faqList = [
      'What the first step of the home buying process?',
      'How long does it take to buy a home?',
      "What is a seller's market?",
      "What is a buyer's market?",
      'How much do i have to pay an agent to help me buy a house?',
      'Should i sell my current home before buying a new one?'
    ];

    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size(double.infinity, 200),
            child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('lib/assets/images/1.png'),
                        fit: BoxFit.cover)),
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                    child: Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Column(children: [
                          SizedBox(height: 20),
                          Row(children: [
                            Container(
                                decoration: BoxDecoration(
                                    color: AppColors.black200,
                                    borderRadius: BorderRadius.circular(30)),
                                child: IconButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(Icons.arrow_back),
                                    color: AppColors.white)),
                            SizedBox(width: 30),
                            Text('F.A.Q',
                                style: AppTextStyles.mediumMedium(context,
                                    color: AppColors.white))
                          ]),
                          SizedBox(height: 20),
                          Padding(
                              padding: EdgeInsets.all(8.0),
                              child: SearchInput(
                                  searchController: searchController,
                                  hintText: 'Search question...'))
                        ]))))),
        body: SingleChildScrollView(
            child: Container(
                child: Column(
                    children: List.generate(
                        faqList.length,
                        (index) => ListTile(
                            leading: Text(faqList[index]),
                            trailing: Icon(Icons.arrow_drop_up)))))));
  }
}
