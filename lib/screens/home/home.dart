import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: AppColors.greyLight, body: Body());
  }
}
