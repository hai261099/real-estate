import 'package:flutter/material.dart';
import 'package:real_estate/screens/home/components/title.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/text_utils.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(TextUtils.text_004,
                      style: AppTextStyles.mediumLarge(context)),
                  SizedBox(height: 20),
                  Column(mainAxisSize: MainAxisSize.min, children: [
                    TypeTitle(title: TextUtils.text_016),
                    SizedBox(height: 15),
                    Container(
                        height: 200,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 5,
                            shrinkWrap: true,
                            itemBuilder: (context, index) => Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.white,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              child: Image.asset(
                                                  'lib/assets/images/intro_1.png'),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  2 /
                                                  5,
                                              height: 120),
                                          SizedBox(height: 5),
                                          Text('55 Giải Phóng',
                                              style: AppTextStyles.mediumMedium(
                                                  context)),
                                          Text('100.000.000đ/tháng'),
                                          SizedBox(height: 5),
                                          Text('Aparment')
                                        ])))))
                  ]),
                  SizedBox(height: 20),
                  Column(children: [
                    TypeTitle(title: TextUtils.text_018),
                    SizedBox(height: 15),
                    Container(
                        height: 200,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 5,
                            shrinkWrap: true,
                            itemBuilder: (context, index) => Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.white,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              child: Image.asset(
                                                  'lib/assets/images/intro_1.png'),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  2 /
                                                  5,
                                              height: 120),
                                          SizedBox(height: 5),
                                          Text('55 Giải Phóng',
                                              style: AppTextStyles.mediumMedium(
                                                  context)),
                                          Text('100.000.000đ/tháng'),
                                          SizedBox(height: 5),
                                          Text('Aparment')
                                        ])))))
                  ]),
                  SizedBox(height: 20),
                  Column(children: [
                    TypeTitle(title: TextUtils.text_019),
                    SizedBox(height: 15),
                    Container(
                        height: 200,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 5,
                            shrinkWrap: true,
                            itemBuilder: (context, index) => Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.white,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              child: Image.asset(
                                                  'lib/assets/images/intro_1.png'),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  2 /
                                                  5,
                                              height: 120),
                                          SizedBox(height: 5),
                                          Text('55 Giải Phóng',
                                              style: AppTextStyles.mediumMedium(
                                                  context)),
                                          Text('100.000.000đ/tháng'),
                                          SizedBox(height: 5),
                                          Text('Aparment')
                                        ])))))
                  ])
                ])));
  }
}
