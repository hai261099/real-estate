import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/utils/text_utils.dart';

class TypeTitle extends StatelessWidget {
  const TypeTitle({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(title, style: AppTextStyles.mediumXLarge(context)),
      Text(TextUtils.text_017,
          style: AppTextStyles.regularXLarge(context,
              color: AppColors.primaryA400))
    ]);
  }
}
