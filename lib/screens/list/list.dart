import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/components/icon_label.dart';
import 'package:real_estate/screens/apartment_detail/apartment_detail.dart';
import 'package:real_estate/theme/theme.dart';

class ListScreen extends StatelessWidget {
  static String routeName = 'list';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size(double.infinity, 160),
            child: Container(
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(30)),
                    image: DecorationImage(
                      image: AssetImage('lib/assets/images/1.png'),
                      fit: BoxFit.cover,
                    )),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Container(
                            decoration: BoxDecoration(
                                color: AppColors.black200,
                                borderRadius: BorderRadius.circular(30)),
                            child: IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(Icons.arrow_back),
                                color: AppColors.white),
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 35.0),
                              child: Text('HOUSE',
                                  style: AppTextStyles.mediumXLarge(context,
                                      color: AppColors.white)))
                        ])))),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.only(left: 30, right: 30),
                child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: 3,
                    itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.only(top: 5.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .pushNamed(ApartmentDetail.routeName);
                            },
                            child: ClipRect(
                                child: Banner(
                                    message: 'RENT',
                                    location: BannerLocation.topEnd,
                                    color: AppColors.green,
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Stack(children: [
                                          ClipRRect(
                                              child: Image.asset(
                                                  'lib/assets/images/6.png'),
                                              borderRadius:
                                                  BorderRadius.circular(12)),
                                          Positioned(
                                              top: 10,
                                              left: 10,
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                      color:
                                                          AppColors.black400),
                                                  child: Padding(
                                                      padding: EdgeInsets.fromLTRB(
                                                          8, 3, 8, 3),
                                                      child: Text(
                                                          'Gold Apartment',
                                                          style: AppTextStyles
                                                              .mediumMedium(
                                                                  context,
                                                                  color: AppColors
                                                                      .white))))),
                                          Positioned(
                                              bottom: 10,
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      60,
                                                  child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceAround,
                                                      mainAxisSize:
                                                          MainAxisSize.max,
                                                      children: [
                                                        IconAndLabel(
                                                            icon: Icons
                                                                .calendar_today,
                                                            label: '2010'),
                                                        IconAndLabel(
                                                            icon: Icons.grid_on,
                                                            label: '450 sq ft'),
                                                        IconAndLabel(
                                                            icon:
                                                                MaterialCommunityIcons
                                                                    .bed_empty,
                                                            label: '5'),
                                                        IconAndLabel(
                                                            icon: Icons.bathtub,
                                                            label: '5'),
                                                        IconAndLabel(
                                                            icon:
                                                                MaterialCommunityIcons
                                                                    .tag,
                                                            label: '4'),
                                                        Icon(
                                                            MaterialCommunityIcons
                                                                .heart_outline,
                                                            color:
                                                                AppColors.white)
                                                      ])))
                                        ]))))))))));
  }
}
