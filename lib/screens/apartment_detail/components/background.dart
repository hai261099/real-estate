import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/components/icon_label.dart';
import 'package:real_estate/theme/theme.dart';

class Background extends StatelessWidget {
  const Background({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(30)),
            image: DecorationImage(
                image: AssetImage('lib/assets/images/1.png'),
                fit: BoxFit.cover)),
        child: Padding(
            padding: EdgeInsets.fromLTRB(16, 36, 16, 16),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          color: AppColors.black200,
                          borderRadius: BorderRadius.circular(30)),
                      child: IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(Icons.arrow_back),
                          color: AppColors.white)),
                  Container(
                      decoration: BoxDecoration(
                          color: AppColors.black200,
                          borderRadius: BorderRadius.circular(8)),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconAndLabel(
                                icon: Icons.calendar_today, label: '2010'),
                            IconAndLabel(
                                icon: Icons.grid_on, label: '450 sq ft'),
                            IconAndLabel(
                                icon: MaterialCommunityIcons.bed_empty,
                                label: '5'),
                            IconAndLabel(icon: Icons.bathtub, label: '5'),
                            IconAndLabel(
                                icon: MaterialCommunityIcons.tag, label: '4')
                          ]))
                ])));
  }
}
