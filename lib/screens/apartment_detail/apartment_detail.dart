import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:real_estate/components/icon_label.dart';
import 'package:real_estate/screens/apartment_detail/components/background.dart';
import 'package:real_estate/store/app_store.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ApartmentDetail extends StatelessWidget {
  static String routeName = 'apartment-detail';

  @override
  Widget build(BuildContext context) {
    AppStore appStore = AppStore();

    GoogleMapController _googleMapController;

    return Scaffold(
        body: SlidingUpPanel(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30)),
            minHeight: MediaQuery.of(context).size.height / 2,
            maxHeight: MediaQuery.of(context).size.height * 9 / 10,
            panel: Padding(
                padding: EdgeInsets.only(left: 24, right: 24),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    child: Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Text('APARTMENT',
                                            style: AppTextStyles.mediumSmall(
                                                context,
                                                color: AppColors.white))),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: AppColors.black400)),
                                Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.green,
                                        borderRadius:
                                            BorderRadius.circular(30)),
                                    child: IconButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        icon: Icon(
                                            MaterialCommunityIcons.cards_heart),
                                        color: AppColors.white))
                              ]),
                          SizedBox(height: 20),
                          Text('Gold Apartment',
                              style: AppTextStyles.mediumXLarge(context)),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('USD 450.00 /month',
                                    style: AppTextStyles.mediumXLarge(context,
                                        color: AppColors.orange500)),
                                ElevatedButton(
                                    child: Text('RENT'),
                                    onPressed: () {},
                                    style: ElevatedButton.styleFrom(
                                        primary: AppColors.green,
                                        onPrimary: AppColors.white))
                              ]),

                          ///address
                          Column(children: [
                            Row(children: [
                              Icon(Icons.location_on),
                              SizedBox(width: 10),
                              Text('Address',
                                  style: AppTextStyles.bold(context, size: 18))
                            ]),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(height: 15),
                                        Text('Street',
                                            style: AppTextStyles.bold(context,
                                                size: 14)),
                                        Text('55 Giải Phóng'),
                                        SizedBox(height: 10),
                                        Text('City',
                                            style: AppTextStyles.bold(context,
                                                size: 14)),
                                        Text('Hà Nội'),
                                        SizedBox(height: 10),
                                        Text('Country',
                                            style: AppTextStyles.bold(context,
                                                size: 14)),
                                        Text('Việt Nam')
                                      ]),
                                  Spacer(),
                                  Container(
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          24,
                                      height: 150,
                                      child: GoogleMap(
                                          initialCameraPosition:
                                              appStore.initialCameraPosition,
                                          myLocationButtonEnabled: false,
                                          zoomControlsEnabled: false,
                                          onMapCreated: (controller) =>
                                              _googleMapController =
                                                  controller))
                                ])
                          ]),

                          SizedBox(height: 10),

                          ///detail
                          Column(children: [
                            Row(children: [
                              Icon(Icons.business),
                              Text('Details',
                                  style: AppTextStyles.bold(context, size: 18))
                            ]),
                            SizedBox(height: 15),
                            Text(
                                'Gold Apartment, lcoated in the center of Paris, comfortable, cozy, beautiful. It has 5 bedrooms, 5 bathrooms and 1 garage. It covers an area of 450 square meters. It was built in 2010.')
                          ]),

                          SizedBox(height: 10),

                          ///Amenities
                          Column(children: [
                            Row(children: [
                              Icon(MaterialCommunityIcons.tag),
                              Text('Amenities',
                                  style: AppTextStyles.bold(context, size: 18))
                            ]),
                            SizedBox(height: 15),
                            Container(
                                height: 70,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    primary: false,
                                    itemCount: 6,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                          padding: EdgeInsets.only(right: 8.0),
                                          child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: AppColors.black,
                                                      width: 1)),
                                              width: 90,
                                              child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(Icons.wifi),
                                                    Text('Wifi')
                                                  ])));
                                    }))
                          ])
                        ])))),
            body: Background()));
  }
}
