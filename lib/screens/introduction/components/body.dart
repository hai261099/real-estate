import 'package:flutter/material.dart';
import 'package:real_estate/screens/login/login.dart';
import 'package:real_estate/theme/theme.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  PageController pageController = PageController();

  List<String> introductionData = [
    'lib/assets/images/intro_1.png',
    'lib/assets/images/intro_2.png',
    'lib/assets/images/intro_3.png'
  ];

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
        duration: Duration(microseconds: 3000),
        margin: EdgeInsets.only(right: 5),
        height: 5,
        width: currentPage == index ? 20 : 5,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            color: currentPage == index ? AppColors.black : AppColors.grey));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      PageView.builder(
          controller: pageController,
          onPageChanged: (value) {
            setState(() {
              currentPage = value;
            });
          },
          itemCount: introductionData.length,
          itemBuilder: (context, index) =>
              Image.asset(introductionData[index])),
      Positioned(
          bottom: 0,
          width: MediaQuery.of(context).size.width,
          child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(LoginScreen.routeName);
                        },
                        child: Text('SKIP',
                            style: AppTextStyles.regularSmall(context,
                                color: AppColors.white))),
                    Row(
                        children: List.generate(introductionData.length,
                            (index) => buildDot(index: index))),
                    currentPage == 0 || currentPage == 1
                        ? InkWell(
                            onTap: () {
                              setState(() {
                                currentPage += 1;
                                pageController.jumpToPage(currentPage);
                              });
                            },
                            child: Text('NEXT',
                                style: AppTextStyles.regularSmall(context,
                                    color: AppColors.white)))
                        : InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .pushNamed(LoginScreen.routeName);
                            },
                            child: Text('DONE',
                                style: AppTextStyles.regularSmall(context,
                                    color: AppColors.white)))
                  ])))
    ]);
  }
}
