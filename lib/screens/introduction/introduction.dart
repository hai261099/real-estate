import 'package:flutter/material.dart';

import 'components/body.dart';

class Introduction extends StatelessWidget {
  static String routeName = 'introduction';

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Body());
  }
}
