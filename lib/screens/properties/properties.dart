import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/components/icon_label.dart';
import 'package:real_estate/theme/theme.dart';

class PropertiesScreen extends StatelessWidget {
  static String routeName = 'properties';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.blue100,
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16.0),
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    primary: false,
                    shrinkWrap: true,
                    itemCount: 4,
                    itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColors.white),
                            child: Column(children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(16, 12, 16, 0),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text('Rosa Residency',
                                            style: AppTextStyles.mediumMedium(
                                                context)),
                                        Row(children: [
                                          Icon(Icons.apartment),
                                          Text('Apartment')
                                        ])
                                      ])),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(20, 8, 20, 8),
                                  child: Stack(children: [
                                    ClipRRect(
                                        child: Image.asset(
                                            'lib/assets/images/1.png'),
                                        borderRadius: BorderRadius.circular(8)),
                                    Positioned(
                                        top: 10,
                                        right: 10,
                                        child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                color: AppColors.black300),
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 3, right: 3),
                                                child: Row(children: [
                                                  Icon(Icons.location_on,
                                                      color: AppColors.white),
                                                  Text('Barcelona',
                                                      style: AppTextStyles
                                                          .regularSmall(context,
                                                              color: AppColors
                                                                  .white))
                                                ])))),
                                    Positioned(
                                        top: 10,
                                        left: 10,
                                        child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                color: AppColors.black300),
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 3.0, right: 3.0),
                                                child: Text('USD 370 / month',
                                                    style: AppTextStyles
                                                        .regularSmall(context,
                                                            color: AppColors
                                                                .white))))),
                                    Positioned(
                                        bottom: 10,
                                        left: 10,
                                        child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: AppColors.green400),
                                            child: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    5.0, 3, 5, 3),
                                                child: Text('Rent',
                                                    style: AppTextStyles
                                                        .regularMedium(context,
                                                            color: AppColors
                                                                .white)))))
                                  ])),
                              Padding(
                                  padding: EdgeInsets.only(top: 5, bottom: 12),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        IconAndLabel(
                                            icon: Icons.calendar_today,
                                            label: '2010'),
                                        IconAndLabel(
                                            icon: Icons.grid_on,
                                            label: '450 sq ft'),
                                        IconAndLabel(
                                            icon: MaterialCommunityIcons
                                                .bed_empty,
                                            label: '5'),
                                        IconAndLabel(
                                            icon: Icons.bathtub, label: '5'),
                                        IconAndLabel(
                                            icon: MaterialCommunityIcons.tag,
                                            label: '4')
                                      ]))
                            ])))))));
  }
}
