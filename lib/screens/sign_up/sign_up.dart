import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

import 'components/body.dart';

class SignUpScreen extends StatelessWidget {
  static String routeName = 'sign-up';

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: AppColors.primaryA400, body: Body());
  }
}
