import 'package:flutter/material.dart';
import 'package:real_estate/components/image_container.dart';
import 'package:real_estate/components/text_form_field.dart';
import 'package:real_estate/theme/theme.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
          top: 0,
          left: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_1.png')),
      Positioned(
          bottom: 0,
          right: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_2.png')),
      Center(
          child: Container(
              width: MediaQuery.of(context).size.width - 100,
              height: MediaQuery.of(context).size.height * 3 / 4,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: AppColors.white),
              child: Column(children: [
                SizedBox(height: 30),
                Text('Real Estate App',
                    style: AppTextStyles.mediumLarge(context)),
                Padding(
                    padding: const EdgeInsets.only(right: 20, left: 20),
                    child: Form(
                        child: Container(
                            child: Column(children: [
                      BuildTextFormField(
                          labelText: 'Name',
                          icon: Icons.person,
                          obscure: false,
                          type: TextInputType.text),
                      BuildTextFormField(
                          labelText: 'Username',
                          icon: Icons.person_pin_outlined,
                          obscure: false,
                          type: TextInputType.text),
                      BuildTextFormField(
                          labelText: 'Email',
                          icon: Icons.email,
                          obscure: false,
                          type: TextInputType.emailAddress),
                      BuildTextFormField(
                          labelText: 'Password',
                          icon: Icons.remove_red_eye,
                          obscure: true,
                          type: TextInputType.text),
                      BuildTextFormField(
                          labelText: 'Confirm Password',
                          icon: Icons.remove_red_eye,
                          obscure: true,
                          type: TextInputType.text),
                      SizedBox(height: 20),
                      Container(
                          height: 45,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: AppColors.primaryA400),
                              onPressed: () {},
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('SIGNUP',
                                        style: AppTextStyles.regularMedium(
                                            context,
                                            color: AppColors.white)),
                                    SizedBox(width: 5),
                                    Icon(Icons.login)
                                  ])))
                    ])))),
                Expanded(
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('LOGIN')))),
                SizedBox(height: 20)
              ])))
    ]);
  }
}
