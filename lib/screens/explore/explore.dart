import 'package:flutter/material.dart';
import 'package:real_estate/screens/list/list.dart';
import 'package:real_estate/theme/theme.dart';

class ExploreScreen extends StatelessWidget {
  static String routeName = 'explore';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16),
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    primary: false,
                    shrinkWrap: true,
                    itemCount: 4,
                    itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(ListScreen.routeName);
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Stack(children: [
                                ClipRRect(
                                    child:
                                        Image.asset('lib/assets/images/1.png'),
                                    borderRadius: BorderRadius.circular(8)),
                                Positioned(
                                    top: 10,
                                    right: 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: AppColors.black300),
                                        child: Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(8, 3, 8, 3),
                                            child: Row(children: [
                                              Icon(Icons.house,
                                                  color: AppColors.white),
                                              Text('45',
                                                  style: AppTextStyles
                                                      .regularSmall(context,
                                                          color:
                                                              AppColors.white))
                                            ])))),
                                Positioned(
                                    bottom: 10,
                                    left: 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: AppColors.green400),
                                        child: Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(8, 3, 8, 3),
                                            child: Text('HOUSE',
                                                style:
                                                    AppTextStyles.mediumXLarge(
                                                        context,
                                                        color:
                                                            AppColors.white)))))
                              ])),
                        ))))));
  }
}
