import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/theme/theme.dart';

class AgentProfile extends StatelessWidget {
  static String routeName = 'agent-profile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(280),
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30)),
                    image: DecorationImage(
                        image: AssetImage('lib/assets/images/1.png'),
                        fit: BoxFit.cover)),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.black200,
                                      borderRadius: BorderRadius.circular(30)),
                                  child: IconButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      icon: Icon(Icons.arrow_back),
                                      color: AppColors.white)),
                              Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.black200,
                                      borderRadius: BorderRadius.circular(30)),
                                  child: IconButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      icon: Icon(Icons.star_border),
                                      color: AppColors.white)),
                            ],
                          ),
                          Spacer(),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.black,
                                      borderRadius: BorderRadius.circular(15)),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                                    child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Icon(Icons.star,
                                              color: AppColors.white),
                                          Text('4,5',
                                              style: AppTextStyles.regularSmall(
                                                  context,
                                                  color: AppColors.white)),
                                        ]),
                                  ),
                                )
                              ])
                        ])))),
        body: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.all(30.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text('Ellen Grubert',
                  style: AppTextStyles.bold(context, size: 18)),
              Icon(MaterialCommunityIcons.facebook, color: AppColors.blue500),
              Icon(MaterialCommunityIcons.instagram, color: AppColors.purpleIndigo),
              Icon(MaterialCommunityIcons.whatsapp,
                  color: AppColors.colorAccent)
            ]),
            SizedBox(height: 10),
            Row(children: [
              Icon(Icons.phone),
              Text('987-654-321'),
              Spacer(),
              ElevatedButton(
                child: Icon(Icons.contact_mail_outlined),
                onPressed: () {},
                style: ElevatedButton.styleFrom(primary: AppColors.black400),
              )
            ]),
            SizedBox(height: 10),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Icon(Icons.mail),
              Text('haind1@nextpay.vn'),
              Spacer(),
              ElevatedButton(
                child: Icon(Icons.mail_outline),
                onPressed: () {},
                style: ElevatedButton.styleFrom(primary: AppColors.black400),
              )
            ]),
            SizedBox(height: 10),
            Text('About me', style: AppTextStyles.bold(context, size: 16)),
            SizedBox(height: 10),
            Text(
                "Originally from N.Y. Ellen moved to Boston to earn a graduate degree in business at Emerson College. This knowledge, coupled with her under-graduate degree in psychology and teaching has provided her with valuable skills that she daily in real estate negotiations, educating buyers and sellers, and effectively markets team's listings. Ellen has been working in Boston",
                style: TextStyle(wordSpacing: 5))
          ]),
        )));
  }
}
