import 'package:flutter/material.dart';
import 'package:real_estate/components/search_input.dart';
import 'package:real_estate/screens/agents/agents_profile.dart';
import 'package:real_estate/screens/user_profile/user_profile.dart';
import 'package:real_estate/theme/theme.dart';

class AgentsScreen extends StatelessWidget {
  static String routeName = 'agents';

  @override
  Widget build(BuildContext context) {
    final searchController = TextEditingController();

    return Scaffold(
      backgroundColor: AppColors.blue100,
      body: SingleChildScrollView(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
        Padding(
          padding: EdgeInsets.fromLTRB(16.0, 8, 16, 8),
          child: Row(mainAxisSize: MainAxisSize.min, children: [
            Form(
                child: SizedBox(
              width: MediaQuery.of(context).size.width * 4 / 5,
              child: SearchInput(
                  searchController: searchController,
                  hintText: 'Search: name, lastname, ...'),
            )),
            IconButton(icon: Icon(Icons.filter_list), onPressed: () {})
          ]),
        ),
        ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: 4,
            itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.fromLTRB(16.0, 5, 16, 5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: ListTile(
                      leading: ClipOval(
                        child: Image.asset('lib/assets/images/francesco.png'),
                      ),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Ellen Grubert',
                              style: AppTextStyles.mediumMedium(context)),
                          Container(
                            decoration: BoxDecoration(
                                color: AppColors.black,
                                borderRadius: BorderRadius.circular(15)),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                              child: Row(children: [
                                Icon(Icons.star, color: AppColors.white),
                                Text('4,5',
                                    style: AppTextStyles.regularSmall(context,
                                        color: AppColors.white)),
                              ]),
                            ),
                          )
                        ],
                      ),
                      subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Icon(Icons.phone,
                                    size: AppDimens.textSizeXLarge),
                                SizedBox(width: AppDimens.spaceXSmall),
                                Text('0934679247')
                              ],
                            ),
                            Row(
                              children: [
                                Icon(Icons.mail,
                                    size: AppDimens.textSizeXLarge),
                                SizedBox(width: AppDimens.spaceXSmall),
                                Text('haind1@nextpay.vn')
                              ],
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context)
                                      .pushNamed(AgentProfile.routeName);
                                },
                                child: Text('Profile'))
                          ]),
                    ),
                  ),
                ))
      ])),
    );
  }
}
