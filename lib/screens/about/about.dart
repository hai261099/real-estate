import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:real_estate/store/app_store.dart';
import 'package:real_estate/theme/theme.dart';

class AboutScreen extends StatefulWidget {
  static String routeName = 'about';

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  AppStore appStore = AppStore();

  GoogleMapController _googleMapController;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _googleMapController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(250),
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30)),
                    image: DecorationImage(
                        image: AssetImage('lib/assets/images/1.png'),
                        fit: BoxFit.cover)),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Container(
                              decoration: BoxDecoration(
                                  color: AppColors.black200,
                                  borderRadius: BorderRadius.circular(30)),
                              child: IconButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  icon: Icon(Icons.arrow_back),
                                  color: AppColors.white)),
                          Padding(
                              padding: EdgeInsets.only(left: 35.0),
                              child: Container(
                                  height: 160,
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Real Estate Company',
                                            style: AppTextStyles.mediumLarge(
                                                context,
                                                color: AppColors.white)),
                                        Text(
                                            'Best company to rent or buy a property',
                                            style: AppTextStyles.regularSmall(
                                                context,
                                                color: AppColors.white)),
                                        Spacer(),
                                        Row(children: [
                                          Icon(MaterialCommunityIcons.facebook,
                                              color: AppColors.white),
                                          SizedBox(width: 30),
                                          Icon(MaterialCommunityIcons.instagram,
                                              color: AppColors.white),
                                          SizedBox(width: 30),
                                          Icon(MaterialCommunityIcons.whatsapp,
                                              color: AppColors.white)
                                        ])
                                      ])))
                        ])))),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.all(24),
                child: Column(children: [
                  Column(children: [
                    Row(children: [
                      Icon(Icons.business),
                      SizedBox(width: 10),
                      Text('About us',
                          style: AppTextStyles.bold(context, size: 18))
                    ]),
                    Text(
                        'Our company specializes in the sale and rental of houses, offices, apartments and villas in differnent regions of the world. We are pioneers in the real estate market. Our agents are exxtendsively trained to offer you the residence you need for you and your family.',
                        style: AppTextStyles.regularSmall(context))
                  ]),
                  SizedBox(height: 30),
                  Column(children: [
                    Row(children: [
                      Icon(Icons.location_on),
                      SizedBox(width: 10),
                      Text('Address',
                          style: AppTextStyles.bold(context, size: 18))
                    ]),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 15),
                                Text('Street',
                                    style:
                                        AppTextStyles.bold(context, size: 14)),
                                Text('55 Giải Phóng'),
                                SizedBox(height: 10),
                                Text('City',
                                    style:
                                        AppTextStyles.bold(context, size: 14)),
                                Text('Hà Nội'),
                                SizedBox(height: 10),
                                Text('Country',
                                    style:
                                        AppTextStyles.bold(context, size: 14)),
                                Text('Việt Nam')
                              ]),
                          Spacer(),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              height: 200,
                              child: GoogleMap(
                                  initialCameraPosition:
                                      appStore.initialCameraPosition,
                                  myLocationButtonEnabled: false,
                                  zoomControlsEnabled: false,
                                  onMapCreated: (controller) =>
                                      _googleMapController = controller))
                        ])
                  ])
                ]))));
  }
}
