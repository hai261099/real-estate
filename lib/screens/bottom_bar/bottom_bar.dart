import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:real_estate/components/common_app_bar.dart';
import 'package:real_estate/components/drawer.dart';
import 'package:real_estate/screens/explore/explore.dart';
import 'package:real_estate/screens/home/home.dart';
import 'package:real_estate/screens/properties/properties.dart';
import 'package:real_estate/store/bottom_bar_store.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:real_estate/screens/agents/agents.dart';

class BottomBar extends StatefulWidget {
  static String routeName = 'home';

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  BottomBarStore bottomBarStore = BottomBarStore();

  static List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    ExploreScreen(),
    PropertiesScreen(),
    AgentsScreen()
  ];

  static List<String> _titleOptions = ['', 'Explore', 'Properties', 'Agents'];

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      int _selectedIndex = bottomBarStore.selectedItem;
      return Scaffold(
          drawer: DrawerScreen(),
          appBar:
              commonAppBar(_titleOptions.elementAt(_selectedIndex), context),
          body: _widgetOptions.elementAt(_selectedIndex),
          bottomNavigationBar: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(Icons.home),
                    label: 'Home',
                    backgroundColor: AppColors.greyLight),
                BottomNavigationBarItem(
                    icon: Icon(Entypo.compass), label: 'Explore'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.business), label: 'Properties'),
                BottomNavigationBarItem(
                    icon: Icon(MaterialCommunityIcons.account_multiple),
                    label: 'Agents')
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: AppColors.primaryA500,
              onTap: bottomBarStore.onItemSelected,
              selectedLabelStyle: AppTextStyles.regularMedium(context,
                  color: AppColors.primaryA500),
              unselectedLabelStyle:
                  AppTextStyles.regularMedium(context, color: AppColors.grey),
              showSelectedLabels: true,
              showUnselectedLabels: true,
              unselectedItemColor: AppColors.grey));
    });
  }
}
