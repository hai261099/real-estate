import 'package:flutter/material.dart';
import 'package:real_estate/components/text_input.dart';
import 'package:real_estate/theme/theme.dart';

class UserProfile extends StatelessWidget {
  static String routeName = 'user-profile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.greyLight,
        appBar: AppBar(
          backgroundColor: AppColors.greyLight,
          title: Text('Profile', style: AppTextStyles.mediumXLarge(context)),
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(color: AppColors.primaryA400),
        ),
        body: SizedBox(
          width: double.infinity,
          child: SingleChildScrollView(
              child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Form(
                      child: Column(children: [
                    Stack(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 1 / 3,
                          height: MediaQuery.of(context).size.width * 1 / 3,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 4), // changes position of shadow
                                )
                              ],
                              color: AppColors.black,
                              borderRadius: BorderRadius.circular(8)),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(
                                color: AppColors.red300,
                                borderRadius: BorderRadius.circular(30)),
                            child: IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.edit),
                                color: AppColors.white),
                          ),
                        )
                      ],
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 16.0, bottom: 8),
                        child: TextInput(
                            icon: Icons.person_outline, labelText: 'Name')),
                    TextInput(icon: Icons.person, labelText: 'Username'),
                    Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 8),
                        child: TextInput(
                            icon: Icons.mail_outline, labelText: 'Email')),
                    TextInput(icon: Icons.remove_red_eye, labelText: 'Password')
                  ])),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: AppColors.mediumPurple,
                        textStyle: AppTextStyles.regularSmall(context,
                            color: AppColors.white)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('SAVE'),
                        SizedBox(width: 5),
                        Icon(Icons.save)
                      ],
                    ),
                    onPressed: () {},
                  )
                ]),
          )),
        ));
  }
}
