import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

import 'components/body.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static String routeName = 'forgot-password';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.primaryA400,
        body: Body());
  }
}

