import 'package:flutter/material.dart';
import 'package:real_estate/components/image_container.dart';
import 'package:real_estate/components/text_form_field.dart';
import 'package:real_estate/theme/theme.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
          top: 30,
          left: 20,
          child: Container(
              decoration: BoxDecoration(
                  color: AppColors.black200,
                  borderRadius: BorderRadius.circular(30)),
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: AppColors.white))),
      Positioned(
          right: 0,
          top: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_2.png')),
      Positioned(
          bottom: 0,
          left: 0,
          child: BuildImageContainer(image: 'lib/assets/images/intro_2.png')),
      Center(
          child: Container(
              height: MediaQuery.of(context).size.height * 9 / 20,
              width: MediaQuery.of(context).size.width - 10,
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(5)),
              child: Padding(
                  padding: EdgeInsets.all(25.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Password Recovery',
                            style: AppTextStyles.mediumXLarge(context)),
                        SizedBox(height: 15),
                        Text(
                            "Enter the email associated with your account and we'll send you an email with instructions to reset your password",
                            style: AppTextStyles.mediumSmall(context,
                                color: AppColors.grey)),
                        SizedBox(height: 20),
                        Form(
                            child: Column(children: [
                          BuildTextFormField(
                              icon: Icons.email,
                              labelText: 'Email',
                              obscure: false,
                              type: TextInputType.emailAddress),
                          SizedBox(height: 15),
                          Container(
                              height: 45,
                              child: ElevatedButton(
                                  onPressed: () {},
                                  style: ElevatedButton.styleFrom(
                                      textStyle: AppTextStyles.regularMedium(
                                          context,
                                          color: AppColors.white)),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('SEND EMAIL'),
                                        SizedBox(width: 5),
                                        Icon(Icons.send)
                                      ])))
                        ]))
                      ]))))
    ]);
  }
}
