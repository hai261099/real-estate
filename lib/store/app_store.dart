import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobx/mobx.dart';

part 'app_store.g.dart';

class AppStore = _AppStore with _$AppStore;

abstract class _AppStore with Store {
  var initialCameraPosition = CameraPosition(
      target: LatLng(21.002942620147, 105.84315643473), zoom: 11.5);
}
