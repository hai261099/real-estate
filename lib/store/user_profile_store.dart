import 'package:mobx/mobx.dart';

part 'user_profile_store.g.dart';

class UserProfileStore = _UserProfileStore with _$UserProfileStore;

abstract class _UserProfileStore with Store {}
