import 'package:mobx/mobx.dart';

part 'bottom_bar_store.g.dart';

class BottomBarStore = _BottomBarStore with _$BottomBarStore;

abstract class _BottomBarStore with Store {
  @observable
  int selectedItem = 0;

  @action
  onItemSelected(index) {
    selectedItem = index;
  }
}
