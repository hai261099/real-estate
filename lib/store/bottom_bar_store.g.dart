// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottom_bar_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BottomBarStore on _BottomBarStore, Store {
  final _$selectedItemAtom = Atom(name: '_BottomBarStore.selectedItem');

  @override
  int get selectedItem {
    _$selectedItemAtom.reportRead();
    return super.selectedItem;
  }

  @override
  set selectedItem(int value) {
    _$selectedItemAtom.reportWrite(value, super.selectedItem, () {
      super.selectedItem = value;
    });
  }

  final _$_BottomBarStoreActionController =
      ActionController(name: '_BottomBarStore');

  @override
  dynamic onItemSelected(dynamic index) {
    final _$actionInfo = _$_BottomBarStoreActionController.startAction(
        name: '_BottomBarStore.onItemSelected');
    try {
      return super.onItemSelected(index);
    } finally {
      _$_BottomBarStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selectedItem: ${selectedItem}
    ''';
  }
}
